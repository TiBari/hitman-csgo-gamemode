#pragma semicolon 1

#define PLUGIN_AUTHOR "Rachnus & September & White Wolf"
#define PLUGIN_VERSION "1.42"
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <emitsoundany>
#include <clientprefs>
#include <csgo_colors>
#include <vip_core>

#pragma newdecls required

EngineVersion g_Game;
#define HITMAN_PREFIX "[\x04Хитман\x01]"
#define HIDE_RADAR 1<<12
#define GRAB_DISTANCE 100.0

#define AGENT47_MODEL "models/player/custom_player/voikanaa/hitman/agent47.mdl"
#define AGENT47_HEARTBEAT "hitmancsgo/heartbeat.mp3"
#define AGENT47_LOCATED "hitmancsgo/located.mp3"
#define AGENT47_SPOTTED "hitmancsgo/spotted.mp3"
#define AGENT47_DISGUISE "hitmancsgo/disguise.mp3"
#define AGENT47_SELECTED "hitmancsgo/selected.mp3"
#define MINE_ACTIVE "hitmancsgo/mine_activate.mp3"
#define MINE_EXPLODE "weapons/sensorgrenade/sensor_explode.wav"
#define TIMER_SOUND "buttons/button17.wav"
#define MINE_DEPLOY_VOLUME 0.0
#define MINE_EXPLOSION_VOLUME 1.0
#define MODEL_BEAM	"materials/sprites/purplelaser1.vmt"

#define IDENTITY_SCAN_SOUND "buttons/blip2.wav"
#define IDENTITY_SCAN_VOLUME 1.0

static const char g_sFeature[] = "hitmanrole";

//HITMAN VARIABLES
int g_iHitman = INVALID_ENT_REFERENCE;
int g_iHitmanGlow = INVALID_ENT_REFERENCE;
int g_iHitmanTarget = INVALID_ENT_REFERENCE;
int g_iHitmanTargetGlow = INVALID_ENT_REFERENCE;
int g_iHitmanKiller = INVALID_ENT_REFERENCE;
int g_iHitmanTimer = 0;
int g_iHitmanTripmines = 0;
int g_iHitmanMaxTripmines = 0;
int g_iHitmanDecoys = 0;
int g_iHitmanMaxDecoys = 0;
int g_iHitmanTargetKills = 0;
int g_iHitmanNonTargetKills = 0;
bool g_bHasHitmanBeenSpotted = false;
bool g_bIsHitmanSeen = false;
bool g_bHitmanWasSeen = false;
bool g_bHitmanWasInOpen = false;
bool g_bHitmanPressedAttack1 = false;
bool g_bHitmanPressedUse = false;
char g_iHitmanDisguise[MAX_NAME_LENGTH];

//OTHER VARIABLES
int g_iDecoys[MAXPLAYERS + 1] =  { false, ... };
int g_iMaxDecoys;
int g_iGrabbed[MAXPLAYERS + 1] =  { INVALID_ENT_REFERENCE, ... };
int g_iPlayersOnStart;
int g_iPathLaserModelIndex;
int g_iPathHaloModelIndex;
int g_iDPI[MAXPLAYERS + 1];
bool g_bNotifyHelp[MAXPLAYERS + 1] =  { false, ... };
bool g_bNotifyHitmanInfo[MAXPLAYERS + 1] =  { false, ... };
bool g_bNotifyTargetInfo[MAXPLAYERS + 1] =  { false, ... };
bool g_bPressedAttack2[MAXPLAYERS + 1] =  { false, ... };
bool g_bDidHitHitman[MAXPLAYERS + 1] =  { false, ... };
bool g_bxD[MAXPLAYERS+1] = {false, ...};
bool g_bPickingHitman = false;

bool g_bFailedPickupHitman = false;

int g_iNextHitman = 0;

ArrayList g_iRagdolls;
ArrayList g_iMines;
UserMsg g_BombText;
Handle g_hNotifyHelp = null;
Handle g_hNotifyHitmanInfo = null;
Handle g_hNotifyTargetInfo = null;
KeyValues g_Weapons;

// TIMERS
Handle g_PickHitmanTimer;
Handle g_hDecoyDispatchTimer;

//CONVARS
ConVar g_TimeUntilHitmanChosen;
ConVar g_RoundEndTime;
ConVar g_DamagePenalty;
ConVar g_PenaltyType;
ConVar g_RagdollLimit;
ConVar g_MineExplosionRadius;
ConVar g_MineExplosionDamage;
ConVar g_IdentityScanRadius;
ConVar g_AllowTargetsGrabRagdoll;
ConVar g_HelpNoticeTime;
ConVar g_HitmanArmor;
ConVar g_HitmanHasHelmet;
ConVar g_TargetsArmor;
ConVar g_TargetsHaveHelmet;
ConVar g_DisguiseOnStart;
ConVar g_PunishOnFriendlyFire;
ConVar g_PunishOnFriendlyFireDamage;
ConVar g_AllowRemovalOfSilencer;
ConVar g_DefaultHitmanChance;
ConVar g_hDecoyDispatchTimerCvar;

//FORWARDS
Handle g_hOnPickHitman;
Handle g_hOnPickHitmanTarget;

// Array for round logs
ArrayList g_aLogs;

// Map for last roles of players
Handle IDPICookie;
int ipdi_t_againafter;

// Netprops
int m_bHasHelmet;
int m_bAlive;
int m_iTeam;
int m_iPendingTeam;
int m_iArmor;
int m_iKills;
int m_iAssists;
int m_iDeaths;

public Plugin myinfo = 
{
	name = "Hitman Mod",
	author = PLUGIN_AUTHOR,
	description = "A hitman mode for CS:GO",
	version = PLUGIN_VERSION,
	url = "https://github.com/Rachnus"
};

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO)
	{
		SetFailState("This plugin is for CSGO");	
	}
	//CONVARS
	g_TimeUntilHitmanChosen =	 	CreateConVar("hitmancsgo_time_until_hitman_chosen", "20", "Time in seconds until a hitman is chosen on round start");
	g_RoundEndTime =			 	CreateConVar("hitmancsgo_round_end_timer", "3", "Time in seconds until next round starts after round ends when hitman is killed or targets killed");
	g_DamagePenalty = 			 	CreateConVar("hitmancsgo_damage_penalty", "25", "Amount of damage to hurt hitman if wrong target was killed");
	g_PenaltyType = 			 	CreateConVar("hitmancsgo_penalty_type", "1", "0 = No punishment, 1 = Punish hitman on wrong target kill using hitmancsgo_damage_penalty, 2 = Reflect the damage onto hitman if wrong target was shot",0,true,0.0,true,2.0);
	g_RagdollLimit = 			 	CreateConVar("hitmancsgo_ragdoll_limit", "10", "Amount of server side ragdolls there can be (Lower this if server starts lagging)");
	g_MineExplosionRadius =		 	CreateConVar("hitmancsgo_explosion_radius", "600", "Tripmine explosion radius");
	g_MineExplosionDamage =		 	CreateConVar("hitmancsgo_explosion_damage", "500", "Tripmine explosion damage (magnitude)");
	g_IdentityScanRadius = 		 	CreateConVar("hitmancsgo_identity_scan_radius", "400", "Identity scan grenade radius");
	g_AllowTargetsGrabRagdoll =  	CreateConVar("hitmancsgo_allow_targets_grab_ragdolls", "0", "Allow non hitman to grab ragdolls (Might cause lag if many players grab ragdolls)");
	g_HelpNoticeTime = 			 	CreateConVar("hitmancsgo_notice_timer", "20", "Time in seconds to notify players gamemode information");
	g_HitmanArmor = 			 	CreateConVar("hitmancsgo_hitman_armor", "100", "Amount of armor hitman should have");
	g_HitmanHasHelmet =			 	CreateConVar("hitmancsgo_hitman_has_helmet", "1", "Should hitman have helmet");
	g_TargetsArmor =			 	CreateConVar("hitmancsgo_targets_armor", "100", "Amount of armor targets should have");
	g_TargetsHaveHelmet = 		 	CreateConVar("hitmancsgo_targets_have_helmet", "0", "Should targets have helmets");
	g_DisguiseOnStart =			 	CreateConVar("hitmancsgo_disguise_hitman_on_round_start", "1", "Should the hitman be disguised on round start");
	g_PunishOnFriendlyFire =	 	CreateConVar("hitmancsgo_punish_on_friendly_fire", "1", "Should targets get damaged if they kill another target?");
	g_PunishOnFriendlyFireDamage =	CreateConVar("hitmancsgo_punish_on_friendly_fire_damage", "50.0", "Amount of damage to deal if 'hitmancsgo_punish_on_friendly_fire' is set to 1");
	g_AllowRemovalOfSilencer =		CreateConVar("hitmancsgo_allow_removal_of_silencer", "0", "Should silencers (M4A1 or USP) be removable");
	g_DefaultHitmanChance = 		CreateConVar("hitmancsgo_default_chance", "5", "Default chance to be a hitman, if no VIP privileges.", FCVAR_NONE, true, 1.0, true, 100.0);
	g_hDecoyDispatchTimerCvar =		CreateConVar("hitmancsgo_dispatch_decoy_timer", "8.0", "Time in seconds when decoy must be given to players after weapons was given", FCVAR_NONE, true, 1.0);
	
	g_hNotifyHelp = RegClientCookie("HMGO_Notify_Help", "Уведомлять игроков о правилах мода", CookieAccess_Public);
	g_hNotifyHitmanInfo =  RegClientCookie("HMGO_Notify_Hitman_Info", "Уведомлять игроков о правилах хитмана", CookieAccess_Public);
	g_hNotifyTargetInfo =  RegClientCookie("HMGO_Notify_Target_Info", "Уведомлять игроков о правилах целей", CookieAccess_Public);
	
	g_hOnPickHitman = CreateGlobalForward("HitmanGO_OnHitmanPicked", ET_Ignore, Param_Cell);
	g_hOnPickHitmanTarget = CreateGlobalForward("HitmanGO_OnHitmanTargetPicked", ET_Ignore, Param_Cell);
	
	m_bHasHelmet = FindSendPropInfo("CCSPlayerResource", "m_bHasHelmet");
	m_bAlive = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	m_iTeam = FindSendPropInfo("CCSPlayerResource", "m_iTeam");
	m_iPendingTeam = FindSendPropInfo("CCSPlayerResource", "m_iPendingTeam");
	m_iArmor = FindSendPropInfo("CCSPlayerResource", "m_iArmor");
	m_iKills = FindSendPropInfo("CCSPlayerResource", "m_iKills");
	m_iAssists = FindSendPropInfo("CCSPlayerResource", "m_iAssists");
	m_iDeaths = FindSendPropInfo("CCSPlayerResource", "m_iDeaths");

	RegAdminCmd("sm_hmgorefresh", Command_Refresh, ADMFLAG_ROOT, "Refresh weapons config");
	RegAdminCmd("sm_gethitman", Command_GetHitman, ADMFLAG_BAN, "Get hitman player");
	RegAdminCmd("sm_sethitman", Command_SetNextHitman, ADMFLAG_CONVARS, "Set user to be next hitman");
	
	RegConsoleCmd("sm_hmgohelp", Command_Help);
	RegConsoleCmd("sm_h", Command_Help);
	
	RegConsoleCmd("sm_hitmaninfo", Command_HitmanInfo);
	RegConsoleCmd("sm_hi", Command_HitmanInfo);
	
	RegConsoleCmd("sm_targetinfo", Command_TargetInfo);
	RegConsoleCmd("sm_ti", Command_TargetInfo);
	
	RegConsoleCmd("sm_hmgonotify", Command_Notify);
	RegConsoleCmd("sm_hn", Command_Notify);
	
	RegAdminCmd("sm_logs", Command_Logs, ADMFLAG_BAN, "Hitman logs of round");
	
	HookEvent("player_spawn", Event_PlayerSpawn, EventHookMode_Post);
	HookEvent("player_death", Event_PlayerDeath, EventHookMode_Pre);
	HookEvent("player_hurt", Event_PlayerHurt);
	HookEvent("weapon_fire", Event_WeaponFire);
	HookEvent("decoy_started", Event_DecoyStarted);
	HookEvent("decoy_firing", Event_DecoyFiring, EventHookMode_Post);
	HookEvent("round_prestart", Event_RoundStart);
	HookEvent("round_poststart", Event_RoundPostStart);
	HookEvent("round_end", Event_RoundEnd);
	
	LoadTranslations("common.phrases");
	
	AddCommandListener(Command_JoinTeam, "jointeam");
	
	g_BombText = GetUserMessageId("TextMsg");
	HookUserMessage(g_BombText, UserMessageHook, true);

	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "configs/hitmancsgo_weapons.cfg");
	g_Weapons = new KeyValues("weapons");
	
	if(!g_Weapons.ImportFromFile(path))
		SetFailState("Could not open %s", path);
	g_Weapons.SetEscapeSequences(true);
	
	g_iRagdolls = new ArrayList();
	g_iMines = new ArrayList();
	
	g_aLogs = new ArrayList(512);
	IDPICookie = RegClientCookie("HMGO_againafter", "Can be hitman again after X rounds, this is X", CookieAccess_Private);
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
			OnClientPutInServer(i);
	}
	AutoExecConfig(true, "hitmancsgo");
	
	if(VIP_IsVIPLoaded())
	{
		VIP_OnVIPLoaded();
	}
}

public void VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(g_sFeature, INT);
}

public void OnPluginEnd()
{
	VIP_UnregisterMe();
}

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int err_max)
{
	CreateNative("HitmanGO_GetHitman", Native_GetHitman);
	CreateNative("HitmanGO_GetHitmanKiller", Native_GetHitmanKiller);
	CreateNative("HitmanGO_HasHitmanBeenSpotted", Native_HasHitmanBeenSpotted);
	CreateNative("HitmanGO_GetCurrentHitmanTarget", Native_GetCurrentHitmanTarget);
	CreateNative("HitmanGO_GetRoundStartPlayers", Native_GetRoundStartPlayers);
	CreateNative("HitmanGO_GetTargetKills", Native_GetTargetKills);
	CreateNative("HitmanGO_GetNonTargetKills", Native_GetNonTargetKills);
	CreateNative("HitmanGO_IsHitmanSeen", Native_IsHitmanSeen);
	CreateNative("HitmanGO_IsValidHitman", Native_IsValidHitman);

	RegPluginLibrary("hitmancsgo");

	return APLRes_Success;
}

public int Native_GetHitman(Handle plugin, int numParams)
{
	int hitman = GetClientOfUserId(g_iHitman);
	return hitman;
}

public int Native_GetHitmanKiller(Handle plugin, int numParams)
{
	int hitmankiller = GetClientOfUserId(g_iHitmanKiller);
	return hitmankiller;
}

public int Native_HasHitmanBeenSpotted(Handle plugin, int numParams)
{
	return view_as<int>(g_bHasHitmanBeenSpotted);
}

public int Native_GetCurrentHitmanTarget(Handle plugin, int numParams)
{
	int hitmantarget = GetClientOfUserId(g_iHitmanTarget);
	return hitmantarget;
}

public int Native_GetRoundStartPlayers(Handle plugin, int numParams)
{
	return g_iPlayersOnStart;
}

public int Native_GetTargetKills(Handle plugin, int numParams)
{
	return g_iHitmanTargetKills;
}

public int Native_GetNonTargetKills(Handle plugin, int numParams)
{
	return g_iHitmanNonTargetKills;
}

public int Native_IsHitmanSeen(Handle plugin, int numParams)
{
	return view_as<int>(g_bIsHitmanSeen);
}

public int Native_IsValidHitman(Handle plugin, int numParams)
{
	return view_as<int>(IsValidHitman());
}

/*************
 * CALLBACKS *
 *************/
 
//Remove bomb pickup/bomb drop notification
public Action UserMessageHook(UserMsg msg_id, Handle pb, const int[] players, int playersNum, bool reliable, bool init)
{
	if(msg_id == g_BombText)
	{
		int msgindex = PbReadInt(pb, "msg_dst");
		if(msgindex == 4)
			return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	// Clear memory leak
	g_iRagdolls.Clear();
	g_iMines.Clear();
	// Clear logs
	g_aLogs.Clear();
	
	ResetVariables();
	TransferPlayersToT();
	g_bPickingHitman = true;
	ipdi_t_againafter = GetRandomInt(0, 6); // 3


	if(GetClientCountWithoutBots() > 0)
 	{
		if(g_PickHitmanTimer != null)
			KillTimer(g_PickHitmanTimer);
		g_PickHitmanTimer = CreateTimer(1.0, Timer_PickHitman, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
 	}
 }
 
 public Action Event_RoundPostStart(Event event, const char[] name, bool dontBroadcast)
{
	SetConVarInt(FindConVar("mp_tagging_scale"), 200);
}


void ExecuteGamemodeCvars()
{
	SetConVarInt(FindConVar("mp_playerid"), 0);
	SetConVarInt(FindConVar("mp_friendlyfire"), 1);
	SetConVarInt(FindConVar("mp_autoteambalance"), 0);
	SetConVarInt(FindConVar("mp_teammates_are_enemies"), 1);
	SetConVarInt(FindConVar("sv_deadtalk"), 0);
	SetConVarInt(FindConVar("sv_alltalk"), 1);
	SetConVarInt(FindConVar("sv_show_team_equipment_prohibit"), 0);
	SetConVarInt(FindConVar("sv_occlude_players"), 0);	
	SetConVarInt(FindConVar("mp_respawn_on_death_t"), 0);
	SetConVarInt(FindConVar("mp_respawn_on_death_ct"), 0);
	SetConVarInt(FindConVar("mp_default_team_winner_no_objective"), 3);
	SetConVarInt(FindConVar("mp_warmuptime"), 20);
	SetConVarInt(FindConVar("mp_buytime"), 0);
	SetConVarInt(FindConVar("mp_buy_anywhere"), 0);
	SetConVarInt(FindConVar("bot_quota"), 0);
	SetConVarInt(FindConVar("mp_solid_teammates"), 1);
	SetConVarInt(FindConVar("sv_talk_enemy_living"), 1);
	SetConVarInt(FindConVar("sv_ignoregrenaderadio"), 1);
	SetConVarInt(FindConVar("mp_randomspawn"), 0);
	SetConVarInt(FindConVar("mp_spawnprotectiontime"), 0);
	SetConVarInt(FindConVar("mp_tagging_scale"), 200);
	SetConVarInt(FindConVar("sv_gameinstructor_disable"), 1);
}

public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	int hitman = GetClientOfUserId(g_iHitman);
	
	if(IsValidHitman(true))
	{
		if(GetAliveCount() > 1)
		{
			//TARGETS WIN
			CGOPrintToChatAll("Игрок {LIGHTBLUE}'%N'{RED} не смог завалить все цели!", hitman);
		}
		else
		{
			if(IsPlayerAlive(hitman))
			{
				if((g_iHitmanTargetKills + g_iHitmanNonTargetKills) >= g_iPlayersOnStart)
					CGOPrintToChatAll("Игрок {LIGHTBLUE}'%N' {GREEN}выполнил задание и устранил все цели!", hitman);
				else
					CGOPrintToChatAll("Игроку {LIGHTBLUE}'%N' {GREEN}успешно удалось устранить все цели!", hitman);
			}
			else
			{
				CGOPrintToChatAll("Игроку {LIGHTBLUE}'%N' {RED} не удалось устранить все цели!", hitman);
			}
		}
		
		PrintToChatAll(" \x04》\x01Убийства целей: \x03%d", g_iHitmanTargetKills);
		PrintToChatAll(" \x04》\x01Убийства без цели: \x03%d", g_iHitmanNonTargetKills);
	}
	
	ResetVariables();
	ClearInventories();
	
	for (int i = 1; i <= MaxClients; ++i)
	{
		if (IsClientInGame(i))
		{
			ShowLogs(i, true);
		}
	}
}

public Action Timer_PickHitman(Handle timer, any entref)
{
	PrintHintTextToAll("<font size='30' color='#FFA500' face=''>ВЫБОР ХИТМАНА ЧЕРЕЗ: <font color='#00FF00'>%d</font>", --g_iHitmanTimer);
 	
 	if(g_iHitmanTimer < -1)
	{
		g_PickHitmanTimer = null;
 		return Plugin_Stop;
	}
 	
 	if(g_iHitmanTimer <= 0)
 	{
 		g_bPickingHitman = false;
 		PickHitman();
		if (g_iHitman != -1) {
			PickHitmanTarget();
		}
		g_PickHitmanTimer = null;
 		return Plugin_Stop;
 	}
	
	EmitSoundToAllAny(TIMER_SOUND, _, SNDCHAN_STATIC,_,_,0.2);
	
	return Plugin_Continue;
}

public Action Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int attacker = GetClientOfUserId(event.GetInt("attacker"));
	int hitman = GetClientOfUserId(g_iHitman);
	int hitmantarget = GetClientOfUserId(g_iHitmanTarget);
	g_iGrabbed[client] = INVALID_ENT_REFERENCE;
	
	if(!IsValidClient(hitman))
		return Plugin_Continue;
	char cBuffer[512];
	FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
	if(IsValidClient(attacker))
	{
		if (attacker == hitman)
		{
			if (client != hitman) // hitman killed somebody and not suicided
			{
				if (client == hitmantarget) // hitman killed victim
				{
					g_iHitmanTargetKills++;
					
					Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] убил цель %N", cBuffer, attacker, client);
					g_aLogs.PushString(cBuffer);
					
					CleanVictimEffects(hitman, client);
				}
				else // hitman killed somebody else
				{
					g_iHitmanNonTargetKills++;
					Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] убил неверную цель %N", cBuffer, attacker, client);
					g_aLogs.PushString(cBuffer);
					
					if(g_PenaltyType.IntValue == 1 && !g_bDidHitHitman[client])
					{
						int armor = GetEntProp(hitman, Prop_Data, "m_ArmorValue");
						SetEntProp(hitman, Prop_Data, "m_ArmorValue", 0);
						SDKHooks_TakeDamage(hitman, 0, 0, g_DamagePenalty.FloatValue, DMG_SHOCK);
						SetEntProp(hitman, Prop_Data, "m_ArmorValue", armor);
						int health = GetEntProp(hitman, Prop_Data, "m_iHealth");
						if(g_DamagePenalty.IntValue >= health)
							PrintHintText(hitman, "<font size='20' color='#FF0000' face=''>ВНИМАНИЕ: <font>Вы умерли!\nУбийство неверных целей приведет к вашей смерти.</font>");
						else
							PrintHintText(hitman, "<font size='20' color='#FF0000' face=''>ВНИМАНИЕ: <font>Вы потеряли %d ХП!\nУбийство неверных целей приведет к вашей смерти.</font>", g_DamagePenalty.IntValue);
					}
					
					if(GetClientCountWithoutBots() <= 1)
					{
						CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_TerroristWin, false);
					}
				}
			}
			else // hitman suicided somehow...
			{
				Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] самоубился", cBuffer, client);
				g_aLogs.PushString(cBuffer);
				
				StopSoundAny(hitman, AGENT47_HEARTBEAT);
				int hitmanglow = EntRefToEntIndex(g_iHitmanGlow);
				if(hitmanglow != INVALID_ENT_REFERENCE)
				{
					AcceptEntityInput(hitmanglow, "Kill");
					g_iHitmanGlow = INVALID_ENT_REFERENCE;
				}	
				if(GetClientCountWithoutBots() > 0)
					CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_CTWin, false);
			}
		}
		else // victim killed by NOT a hitman
		{
			if (client == hitman)
			{
				g_iHitmanKiller = GetClientUserId(attacker);
			
				Format(cBuffer, sizeof(cBuffer), "%s %N убил хитмана %N", cBuffer, attacker, client);
				g_aLogs.PushString(cBuffer);
				
				StopSoundAny(hitman, AGENT47_HEARTBEAT);
				int hitmanglow = EntRefToEntIndex(g_iHitmanGlow);
				if(hitmanglow != INVALID_ENT_REFERENCE)
				{
					AcceptEntityInput(hitmanglow, "Kill");
					g_iHitmanGlow = INVALID_ENT_REFERENCE;
				}	
				if(GetClientCountWithoutBots() > 0)
					CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_CTWin, false);
				return Plugin_Continue;
			}
			else
			{
				bool isBad = g_PunishOnFriendlyFire.BoolValue && attacker != client;
				if (isBad)
				{
					int armor = GetEntProp(attacker, Prop_Data, "m_ArmorValue");
					SetEntProp(attacker, Prop_Data, "m_ArmorValue", 0);
					SDKHooks_TakeDamage(attacker, 0, 0, g_PunishOnFriendlyFireDamage.FloatValue);
					SetEntProp(attacker, Prop_Data, "m_ArmorValue", armor);
				}
				
				if (client == hitmantarget)
				{
					//g_iHitmanTargetKills++;
				
					Format(cBuffer, sizeof(cBuffer), "%s %N убил невиновного %N [цель] %s %s", cBuffer, attacker, client, (isBad) ? "(BAD ACTION)" : "", (attacker == client) ? "(SUICIDE)" : "");
					g_aLogs.PushString(cBuffer);
					
					CleanVictimEffects(hitman, client);
				}
				else if (attacker == client)
				{
					Format(cBuffer, sizeof(cBuffer), "%s %N умер (SUICIDE)", cBuffer, attacker);
					g_aLogs.PushString(cBuffer);
				}
				else
				{
					Format(cBuffer, sizeof(cBuffer), "%s %N убил невиновного %N %s", cBuffer, attacker, client, (isBad) ? "(BAD ACTION)" : "");
					g_aLogs.PushString(cBuffer);
				}
				
				if(GetClientCountWithoutBots() <= 1)
				{
					CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_TerroristWin, false);
				}
			}
		}
	}
	else
	{
		if (client == hitman)
		{
			Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] умер от чего-то", cBuffer, client);
			g_aLogs.PushString(cBuffer);
			
			StopSoundAny(hitman, AGENT47_HEARTBEAT);
			int hitmanglow = EntRefToEntIndex(g_iHitmanGlow);
			if(hitmanglow != INVALID_ENT_REFERENCE)
			{
				AcceptEntityInput(hitmanglow, "Kill");
				g_iHitmanGlow = INVALID_ENT_REFERENCE;
			}	
			if(GetClientCountWithoutBots() > 0)
				CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_CTWin, false);
			return Plugin_Continue;
		}
		else
		{
			if (client == hitmantarget)
			{
				g_iHitmanTargetKills++;
				
				Format(cBuffer, sizeof(cBuffer), "%s %N [цель] был убит чем-то", cBuffer, client);
				g_aLogs.PushString(cBuffer);
				
				CleanVictimEffects(hitman, client);
			}
			else
			{
				g_iHitmanNonTargetKills++;
				
				Format(cBuffer, sizeof(cBuffer), "%s %N был убит чем-то", cBuffer, client);
				g_aLogs.PushString(cBuffer);
				
				if(GetClientCountWithoutBots() <= 1)
				{
					CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_TerroristWin, false);
				}
			}
		}
	}
	
	event.BroadcastDisabled = true;
	return Plugin_Continue;
}

void CleanVictimEffects(int hitman, int client)
{
	CleanUpRagdolls();

	int glow = EntRefToEntIndex(g_iHitmanTargetGlow);
	if(glow != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(glow, "Kill");
		g_iHitmanTargetGlow = INVALID_ENT_REFERENCE;
	}
	
	float pos[3], angles[3];
	GetClientAbsAngles(client, angles);
	GetClientAbsOrigin(client, pos);
	char modelName[PLATFORM_MAX_PATH], clientName[MAX_NAME_LENGTH];
	GetClientModel(client, modelName, sizeof(modelName));
	GetClientName(client, clientName, sizeof(clientName));
	int ragdoll = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
	int serverdoll = CreateEntityByName("prop_ragdoll");
	g_iRagdolls.Push(EntIndexToEntRef(serverdoll));
	if(ragdoll != INVALID_ENT_REFERENCE)
		AcceptEntityInput(ragdoll, "Kill");
	
	DispatchKeyValue(serverdoll, "targetname", clientName);
	DispatchKeyValue(serverdoll, "model", modelName);
	DispatchKeyValue(serverdoll, "spawnflags", "4");
	SetEntPropEnt(client, Prop_Send, "m_hRagdoll", serverdoll);
	SetEntityModel(serverdoll, modelName);
	DispatchSpawn(serverdoll);
	TeleportEntity(serverdoll, pos, angles, NULL_VECTOR);
	
	if(!PickHitmanTarget() && IsValidClient(hitman))
	{
		if(GetClientCountWithoutBots() > 0)
			CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_TerroristWin, false);
	}
}

public Action Event_PlayerHurt(Event event, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(event.GetInt("attacker"));
	int victim = GetClientOfUserId(event.GetInt("userid"));
	int hitman = GetClientOfUserId(g_iHitman);
	int hitmantarget = GetClientOfUserId(g_iHitmanTarget);
	int dmgdealt = event.GetInt("dmg_health");
	char cWeapon[32];
	event.GetString("weapon", cWeapon, sizeof(cWeapon));
	
	if(victim == hitman)
	{
		g_bDidHitHitman[attacker] = true;
	}
			
	if(g_PenaltyType.IntValue == 2)
	{
		if (IsValidClient(attacker))
		{
			if(attacker == hitman && victim != hitmantarget && !g_bDidHitHitman[victim])
			{
				float dmgfloat = float(dmgdealt); //weird as fuck
				int health = GetEntProp(hitman, Prop_Data, "m_iHealth");
				SDKHooks_TakeDamage(hitman, 0, 0, dmgfloat, DMG_SHOCK);
				if(dmgdealt >= health)
				{
					PrintHintText(hitman, "<font size='20' color='#FF0000' face=''>ВНИМАНИЕ: <font>Вы умерли!\nОгонь по неверным целям.</font>");
				}
				else
				{
					PrintHintText(hitman, "<font size='20' color='#FF0000' face=''>ВНИМАНИЕ: <font>Вы потеряли %d ХП!\nОгонь по неверным целям.</font>", dmgdealt);
				}
			}
		}
	}
	
	if (!g_bPickingHitman)
	{
		char cBuffer[512];
		FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
		
		if (IsValidClient(attacker))
		{
			if (attacker == hitman)
			{
				if (IsValidClient(victim))
				{
					if (victim == hitmantarget)
					{
						Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] нанес урон из %s по %N [цель] (-%d hp)", cBuffer, attacker, cWeapon, victim, dmgdealt);
					}
					else
					{
						Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] нанес урон из %s по %N (-%d hp)", cBuffer, attacker, cWeapon, victim, dmgdealt);
					}
				}
			}
			else
			{
				if (IsValidClient(victim))
				{
					if (victim == hitmantarget)
					{
						Format(cBuffer, sizeof(cBuffer), "%s %N фридамажит из %s по %N [цель] (-%d hp)", cBuffer, attacker, cWeapon, victim, dmgdealt);
					}
					else
					{
						Format(cBuffer, sizeof(cBuffer), "%s %N фридамажит из %s по %N (-%d hp)", cBuffer, attacker, cWeapon, victim, dmgdealt);
					}
				}
			}
		}
		else // attacker is SERVER
		{
			if (IsValidClient(victim))
			{
				if (victim == hitmantarget)
				{
					Format(cBuffer, sizeof(cBuffer), "%s %N [цель] получил урон от мира (-%d hp)", cBuffer, victim, dmgdealt);
				}
				else
				{
					Format(cBuffer, sizeof(cBuffer), "%s %N получил урон от мира (-%d hp)", cBuffer, victim, dmgdealt);
				}
			}
		}
		g_aLogs.PushString(cBuffer);
	}
}
 
public Action Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	g_bDidHitHitman[client] = false;
	
	if(!IsFakeClient(client))
	{
		RequestFrame(HideRadar, client);
	}
	
	StripWeapons(client);
}

public Action Event_WeaponFire(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int hitman = GetClientOfUserId(g_iHitman);
	
	if(client == hitman && !g_bIsHitmanSeen)
	{
		char weaponName[64];
		event.GetString("weapon", weaponName, sizeof(weaponName));
		   
		int weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
		bool silencer = (!!GetEntProp(weapon, Prop_Send, "m_bSilencerOn") || 
						   StrContains(weaponName, "bayonet", false) != -1 || 
						   StrContains(weaponName, "knife", false) != -1 ||
						   StrContains(weaponName, "smokegrenade", false) != -1 ||
						   StrContains(weaponName, "flashbang", false) != -1 ||
						   StrContains(weaponName, "healthshot", false) != -1 ||
						   StrContains(weaponName, "hegrenade", false) != -1 ||
						   StrContains(weaponName, "incgrenade", false) != -1 ||
						   StrContains(weaponName, "molotov", false) != -1 ||
						   StrContains(weaponName, "breachcharge", false) != -1);
		if(!silencer)
		{
			float pos[3], angles[3];
			GetClientAbsOrigin(client, pos);
			GetClientEyeAngles(client, angles);
			angles[2] = 0.0;
			angles[0] = 0.0;
			TeleportHitmanGlow(pos, angles);
		}
	}
}

public Action Event_DecoyStarted(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int hitman = GetClientOfUserId(g_iHitman);
	if(client == hitman)
	{
		float pos[3], angles[3];
		pos[0] = event.GetFloat("x");
		pos[1] = event.GetFloat("y");
		pos[2] = event.GetFloat("z");
		
		GetClientEyeAngles(hitman, angles);
		angles[2] = 0.0;
		angles[0] = 0.0;
		
		if(!g_bIsHitmanSeen)
			TeleportHitmanGlow(pos, angles);
	}
}

public Action Event_DecoyFiring(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int hitman = GetClientOfUserId(g_iHitman);
	if(client == hitman)
	{
		int ent = event.GetInt("entityid");
		if(ent != INVALID_ENT_REFERENCE)
			AcceptEntityInput(ent, "Kill");
	}
}

public void HideRadar(any client)
{
	if (IsClientInGame(client)) {
		SetEntProp(client, Prop_Send, "m_iHideHUD", GetEntProp(client, Prop_Send, "m_iHideHUD") | HIDE_RADAR);
	}
}

public Action Command_JoinTeam(int client, const char[] command, int args)
{
	char arg[PLATFORM_MAX_PATH];
	GetCmdArg(1, arg, sizeof(arg));
	int team = StringToInt(arg);
		
	if(IsPlayerAlive(client))
		return Plugin_Handled;
	
	if(team == CS_TEAM_SPECTATOR)
	{
		ForcePlayerSuicide(client);
		return Plugin_Continue;
	}
		
	CS_SwitchTeam(client, CS_TEAM_T);
	if(g_bFailedPickupHitman || GetClientCountWithoutBots() == 2)
	{
		g_bFailedPickupHitman = false;
		ServerCommand("mp_restartgame 1");
	}
	
	return Plugin_Handled;
}

public Action Command_Refresh(int client, int args)
{
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "configs/hitmancsgo_weapons.cfg");
	g_Weapons = new KeyValues("weapons");
	
	if(!g_Weapons.ImportFromFile(path))
		PrintToServer("[hitmancsgo.smx] Could not open %s", path);
		
	return Plugin_Handled;
}

public Action Command_GetHitman(int client, int args)
{
	if (client && IsClientInGame(client))
	{
		if (!IsValidHitman())
			ReplyToCommand(client, "[HITMAN] Команда пока что недоступна. Подготовка.");
		else if (!IsPlayerAlive(client) || g_bxD[client])
		{
			int ihitman = GetClientOfUserId(g_iHitman);
			if (ihitman > 0 && IsClientInGame(ihitman) && IsPlayerAlive(ihitman))
				ReplyToCommand(client, "[HITMAN] %N является хитманом", ihitman);
			else
				ReplyToCommand(client, "[HITMAN] Не удалось узнать кто хитман");
		}
		else
			ReplyToCommand(client, "[HITMAN] Вы должны быть мертвы, чтобы узнать кто хитман");
	}
	
	return Plugin_Handled;
}

public Action Command_SetNextHitman(int client, int args)
{
	if (!args) {
		if (g_iNextHitman > 0 && IsClientInGame(g_iNextHitman)) {
			ReplyToCommand(client, "[HITMAN] %N will be next hitman", g_iNextHitman);
		}
		else {
			ReplyToCommand(client, "[HITMAN] No overrides on hitman");
		}
	}
	else {
		char cTarget[MAX_NAME_LENGTH];
		GetCmdArg(1, cTarget, sizeof(cTarget));
		int iTarget = FindTarget(client, cTarget, true, false);
		if (!iTarget) {
			ReplyToCommand(client, "[HITMAN] You disabled forcing next hitman");
		}
		else if (iTarget == -1) {
			ReplyToCommand(client, "[HITMAN] %t", "Target is not in game");
		}
		else {
			g_iNextHitman = iTarget;
			ReplyToCommand(client, "[HITMAN] You set %N to be hitman next round", iTarget);
		}
	}
	
	return Plugin_Handled;
}

public Action Command_Help(int client, int args)
{
	PrintToChat(client, "%s Набери !hitmaninfo или !targetinfo для подробной информации", HITMAN_PREFIX);
}

public Action Command_HitmanInfo(int client, int args)
{
	PrintHitmanInfo(client);
}

public Action Command_TargetInfo(int client, int args)
{
	PrintTargetInfo(client);
}

public Action Command_Notify(int client, int args)
{
	g_bNotifyHelp[client] = !g_bNotifyHelp[client];
	CGOPrintToChat(client, "%s Уведомления теперь {GREEN}%s", HITMAN_PREFIX, (g_bNotifyHelp[client]) ? "\x02ВЫКЛ":"\x04ВКЛ");
	SetClientCookie(client, g_hNotifyHelp, (g_bNotifyHelp[client]) ? "1":"0"); 
}

public Action Command_Logs(int client, int args)
{
	if (client && IsClientInGame(client))
	{
		if (IsPlayerAlive(client) && !g_bxD[client])
		{
			ReplyToCommand(client, "Вы должны быть мертвы для просмотра логов");
			return Plugin_Handled;
		}
		
		ShowLogs(client, false);
	}
	
	return Plugin_Handled;
}

bool ShowLogs(int client, bool isRoundEnd)
{
	int iSize = g_aLogs.Length;
	if (iSize == 0)
	{
		PrintToConsole(client, "Логи пока ещё не готовы");
		return false;
	}
	
	PrintToConsole(client, "--------------------------------------");
	PrintToConsole(client, "------------Hitman LOGS---------------");
	if (isRoundEnd)
		PrintToConsole(client, "--------------ROUND END---------------");
	
	ArrayList arr_temp = g_aLogs.Clone();
	
	char cBuffer[512];
	for (int i = 0; i < arr_temp.Length - 1; ++i)
	{
		arr_temp.GetString(i, cBuffer, sizeof(cBuffer));
		Format(cBuffer, sizeof(cBuffer), "|%s", cBuffer);
		
		PrintToConsole(client, cBuffer);
	}
	delete arr_temp;
	
	return true;
}

public bool TraceFilterNotSelfAndParent(int entityhit, int mask, any entity)
{
	int parent = GetEntPropEnt(entity, Prop_Data, "m_hMoveParent");
	if(entityhit > 0 && entityhit != entity && entityhit != parent)
		return true;
	
	return false;
}

public bool TraceFilterNotSelf(int entityhit, int mask, any entity)
{
	if(entityhit >= 0 && entityhit != entity)
		return true;
	
	return false;
}

public void OnThinkPostManager(int entity)
{
	for (int i = 0; i <= MaxClients; i++)
	{
		SetEntData(entity, m_iArmor + i, 0, 4);
		SetEntData(entity, m_iTeam + i, CS_TEAM_T, 4);
		SetEntData(entity, m_iPendingTeam + i, CS_TEAM_T, 4);
		SetEntData(entity, m_bAlive + i, true, 4);
		SetEntData(entity, m_bHasHelmet + i, false, 1);
		SetEntData(entity, m_iKills + i, 66, 4);
		SetEntData(entity, m_iAssists + i, 66, 4);
		SetEntData(entity, m_iDeaths + i, 66, 4);
		//FindDataMapInfo(i, "m_iFrags", PropField_Integer, int& num_bits, int& local_offset)
		if (i > 0 && i <= MaxClients && IsClientInGame(i)) {
			CS_SetClientContributionScore(i, 66);
		}
	}
	SetEntProp(entity, Prop_Send, "m_iPlayerC4", 0);
}

public Action SetTransmitTarget(int entity, int client)
{
	// Show target if hitman
	if(!IsValidHitman())
		return Plugin_Handled;
	int hitman = GetClientOfUserId(g_iHitman);
	return (client == hitman) ? Plugin_Continue : Plugin_Handled;
}

public Action SetTransmitHitman(int entity, int client)
{
	if(!IsValidHitman())
		return Plugin_Handled;
		
	// Show target if hitman
	int hitman = GetClientOfUserId(g_iHitman);
	return (client != hitman) ? Plugin_Continue : Plugin_Handled;
}

public Action OnWeaponCanUse(int client, int weapon)
{
	char className[20];
	if (!GetEntityClassname(weapon, className, sizeof(className)))
		return Plugin_Continue;
	if (strcmp(className, "weapon_breachcharge", false) == 0) {
		if (g_iHitman != INVALID_ENT_REFERENCE) {
			int hitman = GetClientOfUserId(g_iHitman);
			return client == hitman ? Plugin_Continue : Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
}

public Action OnPostThinkPost(int client)
{
	SetEntProp(client, Prop_Send, "m_iAddonBits", 0);
	int entity = EntRefToEntIndex(g_iGrabbed[client]);
	if (entity != INVALID_ENT_REFERENCE)
	{
		float vecView[3], vecFwd[3], vecPos[3], vecVel[3];

		GetClientEyeAngles(client, vecView);
		GetAngleVectors(vecView, vecFwd, NULL_VECTOR, NULL_VECTOR);
		GetClientEyePosition(client, vecPos);
	
		vecPos[0] += vecFwd[0] * GRAB_DISTANCE;
		vecPos[1] += vecFwd[1] * GRAB_DISTANCE;
		vecPos[2] += vecFwd[2] * GRAB_DISTANCE;
		
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", vecFwd);
		SubtractVectors(vecPos, vecFwd, vecVel);
		char classname[PLATFORM_MAX_PATH];
		GetEntityClassname(entity, classname, sizeof(classname));
		if(StrEqual(classname, "prop_ragdoll", false))
			ScaleVector(vecVel, 50.0);
		else
			ScaleVector(vecVel, 10.0);
		
		TeleportEntity(entity, NULL_VECTOR, NULL_VECTOR, vecVel);
	}
}

public void OnClientWeaponSwitchPost(int client, int weaponid)
{
	int hitman = GetClientOfUserId(g_iHitman);
	if(client == hitman)
	{
		char classname[PLATFORM_MAX_PATH];
		GetEntityClassname(weaponid, classname, sizeof(classname));
		if(StrEqual(classname, "weapon_c4", false) ||
		   StrEqual(classname, "weapon_decoy", false))
		{
			PrintActiveHitmanSettings(classname);
		}	
	}
	else
	{
		char classname[PLATFORM_MAX_PATH];
		GetEntityClassname(weaponid, classname, sizeof(classname));
		if(StrEqual(classname, "weapon_decoy", false))
		{
			PrintActiveTargetSettings(client, classname);
		}
		else if(StrEqual(classname, "weapon_c4", false))
		{
			SDKHooks_DropWeapon(client, weaponid);
			AcceptEntityInput(weaponid, "Kill");
		}
	}
}

public Action OnDecoySpawned(int entity)
{
	int owner = GetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity");
	int hitman = GetClientOfUserId(g_iHitman);
	if(owner > 0 && owner <= MaxClients)
	{
		if(owner == hitman)
		{
			g_iHitmanDecoys--;
			if(g_iHitmanDecoys > 0)
			{
				GivePlayerItem(owner, "weapon_decoy");
			}
			PrintActiveHitmanSettings("weapon_decoy");
		}
		else
		{
			g_iDecoys[owner]--;
			if(g_iDecoys[owner] > 0)
			{
				GivePlayerItem(owner, "weapon_decoy");
			}
			SDKHook(entity, SDKHook_TouchPost, DecoyTouchPost);
			PrintActiveTargetSettings(owner, "weapon_decoy");
		}
	}
	
	return Plugin_Continue;
}

public Action DecoyTouchPost(int entity, int other)
{
	if(other == 0)
	{
		if(IsValidHitman())
		{
			float pos[3];
			GetEntPropVector(entity, Prop_Data, "m_vecOrigin", pos);
			CreateIdentityScan(pos);
			AcceptEntityInput(entity, "Kill");
		}
		else
			return Plugin_Continue;
	}
	return Plugin_Continue;
}

public void DownloadFilterCallback(QueryCookie cookie, int client, ConVarQueryResult result, const char[] cvarName, const char[] cvarValue, any value)
{
	if(!StrEqual(cvarValue, "all", false))
	{
		KickClient(client, "Измените квар cl_downloadfilter на 'all'");
	}
}

/*************
 * FUNCTIONS *
 *************/
stock int GetAliveCount()
{
	int count = 0;
	for (int i = 1; i <= MaxClients;i++)
	{
		if(IsClientInGame(i) && IsPlayerAlive(i) && (GetClientTeam(i) == CS_TEAM_T || GetClientTeam(i) == CS_TEAM_CT))
			count++;
	}
	return count;
}

stock bool IsValidClient(int client)
{
	if(client > 0 && client <= MaxClients)
	{
		if(IsClientInGame(client))
			return true;
	}
	return false;
}

stock int GetAliveCountWithoutBots()
{
	int count = 0;
	for (int i = 1; i <= MaxClients;i++)
	{
		if(IsClientInGame(i) && IsPlayerAlive(i) && !IsFakeClient(i) && (GetClientTeam(i) == CS_TEAM_T || GetClientTeam(i) == CS_TEAM_CT))
			count++;
	}
	return count;
}

stock void CreateTargetGlowEntity(int target)
{
	char red[16] = "255 0 0 255";

	int glow = CreateEntityByName("prop_dynamic_override");
	g_iHitmanTargetGlow = EntIndexToEntRef(glow);
	SDKHook(glow, SDKHook_SetTransmit, SetTransmitTarget);
	
	char model[PLATFORM_MAX_PATH];
	GetClientModel(target, model, sizeof(model));
	DispatchKeyValue(glow, "model", model);
	DispatchKeyValue(glow, "disablereceiveshadows", "1");
	DispatchKeyValue(glow, "disableshadows", "1");
	DispatchKeyValue(glow, "solid", "0");
	DispatchKeyValue(glow, "spawnflags", "256");
	DispatchKeyValue(glow, "targetname", "red");
	DispatchSpawn(glow);
	
	SetEntProp(glow, Prop_Send, "m_bShouldGlow", true);
	SetEntPropFloat(glow, Prop_Send, "m_flGlowMaxDist", 10000000.0);
	SetEntPropEnt(glow, Prop_Data, "m_hOwnerEntity", target);
	int iFlags = GetEntProp(glow, Prop_Send, "m_fEffects");
	SetEntProp(glow, Prop_Send, "m_fEffects", iFlags | (1 << 0) | (1 << 4) | (1 << 6) | (1 << 9));
	SetGlowColor(glow, red);
	SetVariantString("!activator");
	AcceptEntityInput(glow, "SetParent", target);
	SetVariantString("primary");
	AcceptEntityInput(glow, "SetParentAttachment", glow);  
}

stock void CreateHitmanGlowEntity(float pos[3], float angles[3], bool bonemerge = false)
{
	if(!IsValidHitman())
		return;
	int hitman = GetClientOfUserId(g_iHitman);
	
	char yellow[16] = "255 255 0 50";

	int glow = CreateEntityByName("prop_dynamic_override");
	SetEntityRenderMode(glow, RENDER_TRANSALPHA);
	SetEntityRenderColor(glow, 255, 255, 255, 50);
	g_iHitmanGlow = EntIndexToEntRef(glow);
	SDKHook(glow, SDKHook_SetTransmit, SetTransmitHitman);
	
	char model[PLATFORM_MAX_PATH];
	GetClientModel(hitman, model, sizeof(model));
	
	DispatchKeyValue(glow, "model", model);
	DispatchKeyValue(glow, "disablereceiveshadows", "1");
	DispatchKeyValue(glow, "disableshadows", "1");
	DispatchKeyValue(glow, "solid", "0");
	DispatchKeyValue(glow, "spawnflags", "8");
	DispatchKeyValue(glow, "targetname", "yellow");
	
	DispatchSpawn(glow);
	g_bHasHitmanBeenSpotted = true;
	SetEntProp(glow, Prop_Send, "m_bShouldGlow", true);
	SetEntPropFloat(glow, Prop_Send, "m_flGlowMaxDist", 10000000.0);
	SetEntPropEnt(glow, Prop_Data, "m_hOwnerEntity", hitman);
	SetGlowColor(glow, yellow);
	if(bonemerge)
	{
		int iFlags = GetEntProp(glow, Prop_Send, "m_fEffects");
		SetEntProp(glow, Prop_Send, "m_fEffects", iFlags | (1 << 0) | (1 << 4) | (1 << 6) | (1 << 9));
		SetVariantString("!activator");
		AcceptEntityInput(glow, "SetParent", hitman);
		SetVariantString("primary");
		AcceptEntityInput(glow, "SetParentAttachment", glow);
	}
	
	EmitSoundToAllAny(AGENT47_LOCATED);
	EmitSoundToClientAny(hitman, AGENT47_SPOTTED);
	TeleportEntity(glow, pos, angles, NULL_VECTOR);
}

stock void TeleportHitmanGlow(float pos[3], float angles[3], bool bonemerge = false)
{
	if(!IsValidHitman())
		return;
		
	int hitman = GetClientOfUserId(g_iHitman);
	int glow = EntRefToEntIndex(g_iHitmanGlow);
	if(glow == INVALID_ENT_REFERENCE)
	{
		CreateHitmanGlowEntity(pos, angles, bonemerge);
	}
	else
	{
		char model[PLATFORM_MAX_PATH];
		GetClientModel(hitman, model, sizeof(model));
		SetEntityModel(glow, model);
		if(bonemerge)
		{
			int iFlags = GetEntProp(glow, Prop_Send, "m_fEffects");
			SetEntProp(glow, Prop_Send, "m_fEffects", iFlags | (1 << 0) | (1 << 4) | (1 << 6) | (1 << 9));
			SetVariantString("!activator");
			AcceptEntityInput(glow, "SetParent", hitman);
			SetVariantString("primary");
			AcceptEntityInput(glow, "SetParentAttachment", glow);
		}
		else
		{
			AcceptEntityInput(glow, "ClearParent");
		}
		TeleportEntity(glow, pos, angles, NULL_VECTOR);
	}
}

stock void TransferPlayersToT()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			if(GetClientTeam(i) == CS_TEAM_CT)
			{
				CS_SwitchTeam(i, CS_TEAM_T);
				CS_RespawnPlayer(i);
			}
		}
	}
}

stock int GetClientCountWithoutBots()
{
	int count = 0;
	for (int i = 1; i <= MaxClients;i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i) && (GetClientTeam(i) == CS_TEAM_T || GetClientTeam(i) == CS_TEAM_CT))
			count++;
	}
	return count;
}

int ipdi_blocksystem(ArrayList choices)
{
	int blockedply = 0;

	for (int i = 0; i < choices.Length; ++i)
	{
		int client = choices.Get(i);
		if (IsClientInGame(client) && !IsFakeClient(client))
		{
			int plyTdata = g_iDPI[client];

			if (plyTdata > ipdi_t_againafter) {
				g_iDPI[client] = 0;
			}
			else if (plyTdata >= 1 && plyTdata <= ipdi_t_againafter) {
				g_iDPI[client] = plyTdata+1;
				blockedply++;
			}
		}
	}

	return blockedply;
}

void setblockdata(int client)
{
	if (g_iDPI[client] == 0)
	{
		g_iDPI[client]++; // = 1
	}
}

int getblockdata(int client)
{
	return g_iDPI[client];
}

int ipdi_getchance(int client)
{
	int custompriority = g_DefaultHitmanChance.IntValue;
	
	if (VIP_IsClientVIP(client) && VIP_IsClientFeatureUse(client, g_sFeature))
	{
		custompriority = VIP_GetClientFeatureInt(client, g_sFeature);
		if (custompriority < 0)
		{
			custompriority = 0;
		}
		if (custompriority > 100)
		{
			custompriority = 100;
		}
	}
	
	return custompriority;
}

void PickHitman()
{
	g_bFailedPickupHitman = false;
	//IPDI_SelectRoles_mixed ported from gmod
	int hitman = -1;
	
	if (g_iNextHitman > 0 && IsClientInGame(g_iNextHitman))
	{
		hitman = g_iNextHitman;
		g_iNextHitman = 0;
	}
	
	if (hitman == -1)
	{
		ArrayList choices = new ArrayList(); // clients, that parcipated in choosing of hitman
		
		int dev = -1;
		// int clientCount = GetClientCountWithoutBots();
		if (GetClientCountWithoutBots() > 1)
		{
			for (int i = 1; i <= MaxClients; ++i)
			{
				if (g_bxD[i])
				{
					dev = i;
				}
				
				if (IsClientInGame(i) && !IsFakeClient(i) && GetClientTeam(i) != CS_TEAM_SPECTATOR && IsPlayerAlive(i) && AreClientCookiesCached(i))
				{
					choices.Push(i);
				} 
			}

			bool debugit = false;
			// int choice_num = choices.Length;
			int t_count = 1; // hitman count
			int plyblocked = ipdi_blocksystem(choices);
			if (plyblocked > (choices.Length - t_count))
			{
				debugit = true;
			}
			
			if (dev != -1)
			{
				PrintToConsole(dev, "[HITMAN-DEV] debugit = %b, plyblocked = %i", debugit, plyblocked);
			}

			// Hitman selection part
			if (choices.Length > 0)
			{
				int ts = 0;
				while (ts < t_count)
				{
					int pick = GetRandomInt(0, choices.Length-1);
					int pply = choices.Get(pick);

					if (IsClientInGame(pply) && !IsFakeClient(pply))
					{
						if (dev != -1)
						{
							PrintToConsole(dev, "[HITMAN-DEV] choices.Get(%i) = %N", pick, pply);
						}
						
						if (debugit) // if everybody can't be hitman, because of block, then allow to be hitman first chosen player
						{
							hitman = pply;
							setblockdata(pply);
							//choices.Erase(pick); // not necessary if we have only 1 hitman
							ts++; // instead of this, we can break this iteration
						}
						else
						{
							if (getblockdata(pply) == 0) // if player allowed to be hitman
							{
								if (dev != -1)
								{
									PrintToConsole(dev, "[HITMAN-DEV] ipdi_getchance(%N) = %i", pply, ipdi_getchance(pply));
								}
								
								if (ipdi_getchance(pply) >= GetRandomInt(1,100)) // if player random chance is good then player became hitman
								{
									hitman = pply;
									setblockdata(pply);
									//choices.Erase(pick); // not necessary if we have only 1 hitman
									ts++; // instead of this, we can break this iteration
								}
							}
							else if (dev != -1)
							{
								PrintToConsole(dev, "[HITMAN-DEV] getblockdata(%N) = %i", pply, getblockdata(pply));
							}
						}
					}
				}
			}
		}
		
		delete choices;
	}
	
	if(hitman > 0)
	{
		Call_StartForward(g_hOnPickHitman);
		Call_PushCell(hitman);
		Call_Finish();
		g_iHitman = GetClientUserId(hitman);
		EquipHitmanWeapons();
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsClientInGame(i) && IsPlayerAlive(i))
			{
				if(i != hitman)
					EquipTargetWeapons(i);
			}
		}
		EquipRandomWeapons();		
		if(g_DisguiseOnStart.BoolValue)
		
			g_iHitmanDisguise = "Terrorist";
		
		else
			SetEntityModel(hitman, AGENT47_MODEL);
			
		EmitSoundToAllAny(AGENT47_SELECTED);
		CGOPrintToChatAll("%s {GREEN} Хитман был выбран!", HITMAN_PREFIX);
		CGOPrintToChat(hitman, " {GREEN} ------- {RED}Вы были выбраны ХИТМАНОМ! {GREEN}-------");
		
		/* Logs */
		char cBuffer[512];
		FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
		Format(cBuffer, sizeof(cBuffer), "%s %N стал хитманом", cBuffer, hitman);
		g_aLogs.PushString(cBuffer);
		
		if(!g_bNotifyHitmanInfo[hitman])
			PrintHitmanInfo(hitman, true);
		//Remove hitman on start
		g_iPlayersOnStart = GetAliveCountWithoutBots() - 1;
		SetEntProp(hitman, Prop_Data, "m_ArmorValue", g_HitmanArmor.IntValue);
		SetEntProp(hitman, Prop_Send, "m_bHasHelmet", g_HitmanHasHelmet.BoolValue);
			
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsClientInGame(i) && IsPlayerAlive(i))
			{
				if(i != hitman)
				{
					SetEntProp(i, Prop_Data, "m_ArmorValue", g_TargetsArmor.IntValue);
					SetEntProp(i, Prop_Send, "m_bHasHelmet", g_TargetsHaveHelmet.BoolValue);
					if(!g_bNotifyTargetInfo[i])
						PrintTargetInfo(i);
				}
			}
		}
	}
	else
	{
		CGOPrintToChatAll("%s {LIGHTBLUE} Недостаточно игроков для выбора хитмана", HITMAN_PREFIX);
		g_bFailedPickupHitman = true;
	}
}

void PrintTargetInfo(int client)
{
	Panel panel = CreatePanel();
	panel.SetTitle("Инфopмaция");
	panel.DrawText("》 Haйдитe Xитмaнa и убeйтe eгo первым");
	panel.DrawText("》 Ha ПKM можно двигать предметы");
	panel.DrawText("》 Xитмaн будет подсвечен, если стреляет без глушителя");
	panel.DrawText("》 Oн мoжeт мacкиpoвaтьcя, нe дoвepяйтe никoмy");
	panel.DrawText("》 Xитмaн мoжeт заминировать место");	
	panel.DrawItem("Закрыть");
	panel.DrawItem("Не показывать больше");
	panel.Send(client, TargetPanelHandler, MENU_TIME_FOREVER);
	delete panel;
}

public int TargetPanelHandler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select && IsClientInGame(param1))
	{
		if (param2 == 2)
		{
			SetClientCookie(param1, g_hNotifyTargetInfo, "1"); 
			g_bNotifyTargetInfo[param1] = true;
		}

		//OnClientCookiesCached(param1);
		
		if(menu != null)
			delete menu;
		
		CGOPrintToChat(param1, "%s Набери {GREEN}'!targetinfo' {DEFAULT} в чате чтобы снова открыть это меню!", HITMAN_PREFIX);
	}
}

public int HitmanPanelHandler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select && IsClientInGame(param1))
	{
		if (param2 == 2)
		{
			SetClientCookie(param1, g_hNotifyHitmanInfo, "1"); 
			g_bNotifyHitmanInfo[param1] = true;
			CGOPrintToChat(param1, "%s Набери {GREEN}'!hitmaninfo' {DEFAULT} в чате чтобы снова открыть это меню!", HITMAN_PREFIX);
		}
		
		//OnClientCookiesCached(param1);
		
		if(menu != null)
			delete menu;
	}
}

void PrintHitmanInfo(int client, bool ishitman = false)
{
	Panel panel = CreatePanel();
	if(ishitman)
		panel.SetTitle("Вы были выбраны хитманом!");
	else
		panel.SetTitle("Информация для Хитмана");
	panel.DrawText("》 Уcтpaнитe cвои цeли незаметно");
	panel.DrawText("》 Стрельба без разброса");
	panel.DrawText("》 Нaжмитe E нa трупах для мacкиpoвки");
	panel.DrawText("》 На ПKМ можно двигать трупы");
	panel.DrawText("》 Без глушителя вас обнаружат");
	if(g_PenaltyType.IntValue != 0)
			panel.DrawText("》 Убийство неверных целей приведет к смерти");
	panel.DrawItem("Закрыть");
	panel.DrawItem("Не показывать больше");
	panel.Send(client, HitmanPanelHandler, MENU_TIME_FOREVER);
	delete panel;
}

void EquipHitmanWeapons()
{
	if(!IsValidHitman())
		return;
		
	int hitman = GetClientOfUserId(g_iHitman);
	g_Weapons.Rewind();
	if(!g_Weapons.JumpToKey("hitman"))
	{
		
		GivePlayerItem(hitman, "weapon_usp_silencer");
		return;
	}
		
	if(g_Weapons.GotoFirstSubKey())
	{
		do
		{
			char weaponName[32];
			g_Weapons.GetSectionName(weaponName, sizeof(weaponName));
			int clip = g_Weapons.GetNum("clip", 1);
			int ammo = g_Weapons.GetNum("ammo", 1);
			
			int weapon;
			if((weapon = GivePlayerItem(hitman, weaponName)) != -1)
			{
				SetEntProp(weapon, Prop_Send, "m_iClip1", clip);
				SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", ammo);
				
				if(StrEqual(weaponName, "weapon_c4", false))
				{
					g_iHitmanTripmines = ammo;
					g_iHitmanMaxTripmines = ammo;
					SetEntProp(weapon, Prop_Send, "m_iClip1", 1);
				}
				else if(StrEqual(weaponName, "weapon_decoy", false))
				{
					g_iHitmanDecoys = ammo;
					g_iHitmanMaxDecoys = ammo;
					SetEntProp(weapon, Prop_Send, "m_iClip1", 1);
				}
			}
			
		} while (g_Weapons.GotoNextKey());
		int wep = GetPlayerWeaponSlot(hitman, 2);
		if(IsValidEntity(wep))
		SetEntPropEnt(hitman, Prop_Data, "m_hActiveWeapon", wep);
	}
	g_Weapons.Rewind();
}

void EquipTargetWeapons(int client)
{
	if(client > 0 && client <= MaxClients)
	{
		if(IsClientInGame(client) && IsPlayerAlive(client))
		{
			g_Weapons.Rewind();
			if(!g_Weapons.JumpToKey("targets"))
				return;
				
			if(g_Weapons.GotoFirstSubKey())
			{
				do
				{
					char weaponName[32];
					g_Weapons.GetSectionName(weaponName, sizeof(weaponName));
					if(StrEqual(weaponName, "random", false))
						continue;
						
					int clip = g_Weapons.GetNum("clip");
					int ammo = g_Weapons.GetNum("ammo");
					
					int weapon;
					if((weapon = GivePlayerItem(client, weaponName)) != -1)
					{
						SetEntProp(weapon, Prop_Send, "m_iClip1", clip);
						//SetEntProp(weapon, Prop_Data, "m_iClip2", ammo);
						SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", ammo);
					}
					
				} while (g_Weapons.GotoNextKey());
			}
			g_Weapons.Rewind();
			
			int wep = GetPlayerWeaponSlot(client, 2);
			if(IsValidEntity(wep))
				SetEntPropEnt(client, Prop_Data, "m_hActiveWeapon", wep);
		}
	}
}

public Action DecoyDispatchHandler(Handle timer, Handle hDp)
{
	int hitman = GetClientOfUserId(g_iHitman);
	DataPack dp = view_as<DataPack>(hDp);
	dp.Reset();
	int ratio = dp.ReadCell();
	int ammo = dp.ReadCell();
	ArrayList players = new ArrayList();
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_T && i != hitman)
			players.Push(i);
	}
	
	int amount = RoundToFloor(float(players.Length) / float(ratio));
	int weapon;
	int random;
	int client;
	for (int i = 0; i < amount; i++)
	{
		random = GetRandomInt(0, players.Length - 1);
		
		client = players.Get(random);
		if ((weapon = GivePlayerItem(client, "weapon_decoy")) != -1)
		{
			g_iDecoys[client] = ammo;
			SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", ammo);
			SetEntProp(weapon, Prop_Send, "m_iClip1", 1);
			players.Erase(random);
		}
	}
	
	delete players;
	g_hDecoyDispatchTimer = null;
	return Plugin_Stop;
}

void EquipRandomWeapons()
{
	int hitman = GetClientOfUserId(g_iHitman);
	ArrayList players = new ArrayList();
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_T && i != hitman)
			players.Push(i);
	}
	
	//EQUIP RANDOM TARGET WEAPONS
	g_Weapons.Rewind();
	if(g_Weapons.JumpToKey("targets"))
	{
		if(g_Weapons.JumpToKey("random"))
		{
			if(g_Weapons.GotoFirstSubKey())
			{
				do
				{
					char weaponName[32];
					g_Weapons.GetSectionName(weaponName, sizeof(weaponName));
					int ratio = g_Weapons.GetNum("ratio");
					int clip = g_Weapons.GetNum("clip", 1);
					int ammo = g_Weapons.GetNum("ammo", 1);
					
					
					if(ratio <= 0)
					{
						PrintToServer("[hitmancsgo.smx] Please fill in ratio field if you're using random section'");
						return;
					}
						
					int amount = RoundToFloor(float(players.Length) / float(ratio));
					
					if (StrEqual(weaponName, "weapon_decoy", false))
					{
						g_iMaxDecoys = ammo;
						if (g_hDecoyDispatchTimer != null)
						{
							KillTimer(g_hDecoyDispatchTimer);
							g_hDecoyDispatchTimer = null;
						}
						DataPack dp = new DataPack();
						dp.WriteCell(ratio);
						dp.WriteCell(ammo);
						g_hDecoyDispatchTimer = CreateTimer(g_hDecoyDispatchTimerCvar.FloatValue, DecoyDispatchHandler, dp, TIMER_DATA_HNDL_CLOSE);
						continue;
					}
					
					for (int i = 0; i < amount; i++)
					{
						int weapon;
						int random;
						int client;
						if ((weapon = GivePlayerItem(client = players.Get(random = GetRandomInt(0, players.Length - 1)) , weaponName)) != -1)
						{
							SetEntProp(weapon, Prop_Send, "m_iClip1", clip);
							
							int wep = GetPlayerWeaponSlot(client, 2);
							if(IsValidEntity(wep))
								SetEntPropEnt(client, Prop_Data, "m_hActiveWeapon", wep);
 							players.Erase(random);
						}
					}

				} while (g_Weapons.GotoNextKey());
			}
		}
	}
	g_Weapons.Rewind();
	delete players;
}

stock void StripWeapons(int client)
{
	int weapon; 
	for(int i = 0; i < 5; i++) 
	{ 
	    if((weapon = GetPlayerWeaponSlot(client, i)) != -1) 
	    { 
	    	char classname[PLATFORM_MAX_PATH];
	    	GetEntityClassname(weapon, classname, sizeof(classname));
	    	if(StrContains(classname, "knife", false) != -1 || StrContains(classname, "bayonet", false) != -1)
	    		continue;
	        SDKHooks_DropWeapon(client, weapon, NULL_VECTOR, NULL_VECTOR); 
	        AcceptEntityInput(weapon, "Kill"); 
	    } 
	}  
}

bool PickHitmanTarget(int client = 0)
{
	if(!IsValidHitman())
		return false;
		
	int hitman = GetClientOfUserId(g_iHitman);
	ArrayList temp = new ArrayList();
 	for (int i = 1; i <= MaxClients; i++)
 	{
		if(i != client && IsClientInGame(i) && GetClientTeam(i) != CS_TEAM_SPECTATOR && IsPlayerAlive(i))
 		{
			if(IsPlayerAlive(i) && i != hitman)
				temp.Push(i);
		}
	}
	
	if(temp.Length > 0)
	{
		int hitmantarget = temp.Get(GetRandomInt(0, temp.Length - 1));
		Call_StartForward(g_hOnPickHitmanTarget);
		Call_PushCell(hitmantarget);
		Call_Finish();
		g_iHitmanTarget = GetClientUserId(hitmantarget);
		if(hitmantarget > 0 && hitmantarget <= MaxClients)
		{
			if(!IsClientInGame(hitmantarget) || !IsPlayerAlive(hitmantarget))
				return false;
				
			char name[MAX_NAME_LENGTH];
			GetClientName(hitmantarget, name, sizeof(name));
			PrintActiveHitmanSettings();
			CGOPrintToChat(hitman, "%s Ваша цель - убить {RED}'%s'", HITMAN_PREFIX, name);
			
			/* Logs */
			char cBuffer[512];
			FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
			Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] получил новую цель - %s", cBuffer, hitman, name);
			g_aLogs.PushString(cBuffer);
			
			CreateTargetGlowEntity(hitmantarget);
			delete temp;
			return true;
		}
	}
	delete temp;
	return false;
}

stock void SetGlowColor(int entity, const char[] color)
{
    char colorbuffers[3][4];
    ExplodeString(color, " ", colorbuffers, sizeof(colorbuffers), sizeof(colorbuffers[]));
    int colors[4];
    for (int i = 0; i < 3; i++)
        colors[i] = StringToInt(colorbuffers[i]);
    colors[3] = 255; // Set alpha
    SetVariantColor(colors);
    AcceptEntityInput(entity, "SetGlowColor");
}

stock void ResetVariables()
{
	int hitman = GetClientOfUserId(g_iHitman);
	StopSoundAny(hitman, AGENT47_HEARTBEAT);
	
	int targetglow = EntRefToEntIndex(g_iHitmanTargetGlow);
	if(targetglow != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(targetglow, "Kill");
		g_iHitmanTargetGlow = INVALID_ENT_REFERENCE;
	}
	
	int hitmanglow = EntRefToEntIndex(g_iHitmanGlow);
	if(hitmanglow != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(hitmanglow, "Kill");
		g_iHitmanGlow = INVALID_ENT_REFERENCE;
	}
	// TIMERS
	if (g_hDecoyDispatchTimer != null)
	{
		KillTimer(g_hDecoyDispatchTimer);
		g_hDecoyDispatchTimer = null;
	}
	
	//HITMAN VARIABLES
	g_iHitman = INVALID_ENT_REFERENCE;
	g_iHitmanKiller = INVALID_ENT_REFERENCE;
	g_iHitmanTimer = g_TimeUntilHitmanChosen.IntValue;
	g_iHitmanTripmines = 0;
	g_iHitmanMaxTripmines = 0;
	g_iHitmanDecoys = 0;
	g_iHitmanMaxDecoys = 0;
	g_iHitmanTargetKills = 0;
	g_iHitmanNonTargetKills = 0;
	g_bIsHitmanSeen = false;
	g_bHitmanWasSeen = false;
	g_bHitmanWasInOpen = false;
	g_bHasHitmanBeenSpotted = false;
	g_bHitmanPressedAttack1 = false;
	g_bHitmanPressedUse = false;
	g_iHitmanDisguise = "";
	
	//OTHER VARIABLES
	g_iMaxDecoys = 0;
	g_iPlayersOnStart = 0;
	g_bPickingHitman = false;
	for (int i = 1; i <= MaxClients; i++)
	{
		g_bPressedAttack2[i] = false;
		g_bDidHitHitman[i] = false;
		g_iDecoys[i] = 0;
		g_iGrabbed[i] = INVALID_ENT_REFERENCE;
	}
}

stock bool IsValidHitman(bool dead = false)
{
	int hitman = GetClientOfUserId(g_iHitman);
	if(hitman > 0 && hitman <= MaxClients)
	{
		if(dead && IsClientInGame(hitman))
			return true;
			
		if(IsClientInGame(hitman) && IsPlayerAlive(hitman))
			return true;
	}
	return false;
}

stock void AddMaterialsFromFolder(char path[PLATFORM_MAX_PATH])
{
	DirectoryListing dir = OpenDirectory(path, true);
	if(dir != null)
	{
		char buffer[PLATFORM_MAX_PATH];
		FileType type;
		
		while(dir.GetNext(buffer, PLATFORM_MAX_PATH, type))
		{
			if(type == FileType_File && ((StrContains(buffer, ".vmt", false) != -1) || (StrContains(buffer, ".vtf", false) != -1) && !(StrContains(buffer, ".ztmp", false) != -1)))
			{
				char fullPath[PLATFORM_MAX_PATH];
				Format(fullPath, sizeof(fullPath), "%s%s", path, buffer);
				AddFileToDownloadsTable(fullPath);
				
				if(!IsModelPrecached(fullPath))
					PrecacheModel(fullPath);
			}
		}
	}
}
 
stock void PrintActiveHitmanSettings(const char[] field = "none")
{
	if(!IsValidHitman())
		return;
	int hitman = GetClientOfUserId(g_iHitman);
	int hitmantarget = GetClientOfUserId(g_iHitmanTarget);
	char targetName[MAX_NAME_LENGTH];
	if(hitmantarget > 0 && hitmantarget <= MaxClients)
		GetClientName(hitmantarget, targetName, sizeof(targetName));
	else
		Format(targetName, sizeof(targetName), "");
		
	if(StrEqual(field, "none", false))
	{
		char modelName[PLATFORM_MAX_PATH];
	
		if(StrEqual(g_iHitmanDisguise, "", false))
		{
			Format(modelName, sizeof(modelName), "None (Agent 47)");
			PrintHintText(hitman, "<font size='16' face=''>Цель: <font color='#FF0000'>%s</font>\n<font size='16' face=''>Маскировка: <font color='#FF0000'>%s</font>", targetName , modelName);
		}
		else
		{
			Format(modelName, sizeof(modelName), "%s", g_iHitmanDisguise);
			PrintHintText(hitman, "<font size='16' face=''>Цель: <font color='#FF0000'>%s</font>\n<font size='16' face=''>Маскировка: <font color='#00FF00'>%s</font>", targetName, modelName);
		}
	}
	else if(StrEqual(field, "weapon_c4", false))
			PrintHintText(hitman, "<font size='16' face=''>Мины: <font color='%s'>%d/%d</font>\n<font size='16' face=''>Инфо: <font color='#FFA500'>ЛКМ ДЛЯ УСТАНОВКИ БОМБЫ.</font>", (g_iHitmanTripmines > 0) ? "#00FF00" : "#FF0000", g_iHitmanTripmines, g_iHitmanMaxTripmines);	
	else if(StrEqual(field, "weapon_decoy", false))
		PrintHintText(hitman, "<font size='16' face=''>Декой: <font color='%s'>%d/%d</font>\n<font size='16' face=''>Инфо: <font color='#FFA500'>Показывает себя там, где брошена эта граната, угол приманки унаследован от вас непосредственно</font>", (g_iHitmanDecoys > 0) ? "#00FF00" : "#FF0000", g_iHitmanDecoys, g_iHitmanMaxDecoys);
}

stock void PrintActiveTargetSettings(int client, const char[] field = "none")
{
	if(StrEqual(field, "weapon_decoy", false))
		PrintHintText(client, "<font size='16' face=''>Identity-Scan Grenade: <font color='%s'>%d/%d</font>\n<font size='16' face=''>Инфо: <font color='#FFA500'>Показывает всех хитманов вокруг области, где эта граната выбрасывается.</font>", (g_iDecoys[client] > 0) ? "#00FF00" : "#FF0000", g_iDecoys[client], g_iMaxDecoys);
}

stock void GrabEntity(int client, int entity)
{
	g_iGrabbed[client] = EntIndexToEntRef(entity);
}

stock void ReleaseEntity(int client)
{
	g_iGrabbed[client] = INVALID_ENT_REFERENCE;
}

stock void CleanUpRagdolls()
{
	if(g_iRagdolls.Length > g_RagdollLimit.IntValue)
	{
		int validRagdolls = 0;
		for (int i = 0; i < g_iRagdolls.Length; i++)
		{
			int ragdoll = EntRefToEntIndex(g_iRagdolls.Get(i));
			if(ragdoll != INVALID_ENT_REFERENCE)
				validRagdolls++;
			else
				g_iRagdolls.Erase(i);
		}
		if(validRagdolls > g_RagdollLimit.IntValue)
		{
			int ent = EntRefToEntIndex(g_iRagdolls.Get(0));
			AcceptEntityInput(ent, "Kill");
			g_iRagdolls.Erase(0);
		}
	}
}

public bool SpawnTripmine()
{
	if(!IsValidHitman())
		return false;
	int hitman = GetClientOfUserId(g_iHitman);
	float slopeAngle[3], eyeAngles[3], direction[3], traceendPos[3], eyePos[3];
	GetClientEyeAngles(hitman, eyeAngles);
	GetClientEyePosition(hitman, eyePos);
	GetAngleVectors(eyeAngles, direction, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(direction, direction);
	
	for( int i = 0; i < 3; i++ )
		eyePos[i] += direction[i] * 1.0;
	
	for( int i = 0; i < 3; i++ )
		direction[i] = eyePos[i] + direction[i] * 80.0;
	
	
	Handle trace = TR_TraceRayFilterEx(eyePos, direction, MASK_ALL, RayType_EndPoint, TraceFilterNotSelfAndParent, hitman);
	if(TR_DidHit(trace))
	{
		TR_GetEndPosition(traceendPos, trace);
		TR_GetPlaneNormal(trace, slopeAngle);
		float angles[3];
		GetVectorAngles(slopeAngle, angles);
		angles[0] += 90.0;
		
		//CREATEMINE
		int mine = CreateEntityByName("prop_physics_override");
		int mineref = EntIndexToEntRef(mine);
		g_iMines.Push(mineref);
		
		DispatchKeyValue(mine, "targetname", "tripmine"); 
		DispatchKeyValue(mine, "spawnflags", "4"); 
		DispatchKeyValue(mine, "Solid", "6");
		DispatchKeyValue(mine, "model", "models/weapons/w_ied_dropped.mdl");
		DispatchKeyValue(mine, "physdamagescale", "1.0");
		DispatchKeyValue(mine, "health", "1");
		DispatchSpawn(mine);
		
		SetEntProp(mine, Prop_Data, "m_takedamage", 2);
		SetEntityMoveType(mine, MOVETYPE_NONE);
		TeleportEntity(mine, traceendPos, angles, NULL_VECTOR);
		
		int entityhit = TR_GetEntityIndex(trace);
		
		if (entityhit > MaxClients)
		{
			SetVariantString("!activator");
			AcceptEntityInput(mine, "SetParent", entityhit);
			
			if(GetEntProp(entityhit, Prop_Data, "m_takedamage") == 2 && GetEntProp(entityhit, Prop_Data, "m_iHealth") > 0)
			{
				SetEntPropEnt(mine, Prop_Send, "m_hOwnerEntity", entityhit);
				SDKHook(entityhit, SDKHook_OnTakeDamage, OnTakeDamage);
			}
			
		}
		SDKHook(mine, SDKHook_OnTakeDamage, OnBombTakeDamage);
		EmitAmbientSoundAny(MINE_ACTIVE, traceendPos, mine,_,_, MINE_DEPLOY_VOLUME);
		
		
		//CREATEBEAM
		int beam = CreateEntityByName("env_beam");
		char color[16] = "255 0 0 255";
		SetEntityModel(beam, MODEL_BEAM); // This is where you would put the texture, ie "sprites/laser.vmt" or whatever.
		DispatchKeyValue(beam, "rendercolor", color );
		DispatchKeyValue(beam, "renderamt", "80");
		DispatchKeyValue(beam, "decalname", "Bigshot"); 
		DispatchKeyValue(beam, "life", "0"); 
		DispatchKeyValue(beam, "TouchType", "0");
		
		float end[3];
		angles[0] -= 90.0;
		Handle tracebeam = TR_TraceRayFilterEx(traceendPos, angles, MASK_ALL, RayType_Infinite, TraceFilterNotSelfAndParent, mine);
		if(TR_DidHit(tracebeam))
			TR_GetEndPosition(end, tracebeam);
			
		DispatchSpawn(beam);
		TeleportEntity(beam, end, NULL_VECTOR, NULL_VECTOR);
		
		SetEntPropEnt(beam, Prop_Send, "m_hAttachEntity", beam);
		SetEntPropEnt(beam, Prop_Send, "m_hAttachEntity", mine, 1);
		SetEntProp(beam, Prop_Send, "m_nNumBeamEnts", 2);
		SetEntProp(beam, Prop_Send, "m_nBeamType", 2);
		
		SetVariantString("!activator");
		AcceptEntityInput(beam, "SetParent", mine);
		
		SetEntPropFloat(beam, Prop_Data, "m_fWidth", 0.5); 
		SetEntPropFloat(beam, Prop_Data, "m_fEndWidth", 0.5); 
		ActivateEntity(beam);
		AcceptEntityInput(beam, "TurnOn");
		CloseHandle(trace);
		return true;
	}
	CloseHandle(trace);
	return false;
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype)
{
	int health = GetEntProp(victim, Prop_Data, "m_iHealth");
	float fHealth = float(health);
	if(damage >= fHealth)
	{
		float pos[3];
		GetEntPropVector(victim, Prop_Data, "m_vecOrigin", pos);
		CreateExplosion(pos);
	}
}

public Action OnBombTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype)
{
	int health = GetEntProp(victim, Prop_Data, "m_iHealth");
	float fHealth = float(health);
	if(damage >= fHealth)
	{
		AcceptEntityInput(victim, "ClearParent");
		float pos[3];
		GetEntPropVector(victim, Prop_Data, "m_vecOrigin", pos);
		CreateExplosion(pos);
	}
}

public Action OnClientTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3])
{
	if(g_bPickingHitman)
	{
		damage = 0.0;
		return Plugin_Changed;
	}
	
	/* Make fists safe for this game */
	if (attacker > 0 && attacker <= MaxClients && IsClientInGame(attacker))
	{
		if (weapon > 0)
		{
			char cWeapon[32];
			if (GetEntityClassname(weapon, cWeapon, sizeof(cWeapon)))
			{
				if (StrEqual(cWeapon, "weapon_fists", false) || (StrContains(cWeapon, "melee", false) != -1))
				{
					damage = 0.0;
					return Plugin_Changed;
				}
			}
		}
	}
	
	return Plugin_Continue;
}

public void TriggerExplosion(int ent)
{
	if(!IsValidEntity(ent))
		return;
	
	float pos[3];
	GetEntPropVector(ent, Prop_Data, "m_vecOrigin", pos);
	EmitAmbientSoundAny(MINE_EXPLODE, pos, ent,_,_, MINE_EXPLOSION_VOLUME);	
	
	AcceptEntityInput(ent, "explode");
	AcceptEntityInput(ent, "Kill");
}

stock void CreateExplosion(float pos[3])
{
	int ent = CreateEntityByName("env_explosion");	
	DispatchKeyValue(ent, "spawnflags", "552");
	DispatchKeyValue(ent, "rendermode", "5");
	DispatchSpawn(ent);
	
	//if(IsValidHitman())
	//{
		//int hitman = GetClientOfUserId(g_iHitman);
		//SetEntPropEnt(ent, Prop_Data, "m_hOwnerEntity", hitman);
	//}

	SetEntProp(ent, Prop_Data, "m_iMagnitude", g_MineExplosionDamage.IntValue);
	SetEntProp(ent, Prop_Data, "m_iRadiusOverride", g_MineExplosionRadius.IntValue);
	TeleportEntity(ent, pos, NULL_VECTOR, NULL_VECTOR);
	
	RequestFrame(TriggerExplosion, ent);
}

stock void CreateIdentityScan(float pos[3])
{
	if(IsValidHitman())
	{
		int color[4] =  { 0, 0, 255, 255 };
		TE_SetupBeamRingPoint(pos, 0.1, g_IdentityScanRadius.FloatValue + 100.0, g_iPathLaserModelIndex, g_iPathHaloModelIndex, 0, 15, 0.4, 5.0, 0.0, color, 50, 0);
		TE_SendToAll();
		EmitAmbientSoundAny(IDENTITY_SCAN_SOUND, pos, _,_,_, IDENTITY_SCAN_VOLUME);
		int hitman = GetClientOfUserId(g_iHitman);
	
		float hitmanPos[3];
		GetClientAbsOrigin(hitman, hitmanPos);
		if(GetVectorDistance(hitmanPos, pos) < g_IdentityScanRadius.FloatValue)
		{
			g_iHitmanDisguise = "";
			SetEntityModel(hitman, AGENT47_MODEL);
		}
	}
}

 /*************
 *  FORWARDS  *
 *************/

public void OnGameFrame()
{
	//UPDATE MINES
	int hitman = GetClientOfUserId(g_iHitman);

	if(IsValidHitman())
	{
		bool didhit = false;
		bool didsee = false;
		if(StrEqual(g_iHitmanDisguise, "", false))
		{
			for (int i = 1; i <= MaxClients; i++)
			{
				if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_T && i != hitman)
				{
					float hitmanEyePos[3], clientEyePos[3];
					GetClientEyePosition(hitman, hitmanEyePos);
					GetClientEyePosition(i, clientEyePos);
	
					Handle trace = TR_TraceRayFilterEx(clientEyePos, hitmanEyePos, MASK_PLAYERSOLID, RayType_EndPoint, TraceFilterNotSelf, i);
					if(TR_DidHit(trace))
					{
						if(TR_GetEntityIndex(trace) == hitman)
						{
							//Now a player view is open to the hitman
							float dirToHitman[3], playerDir[3];
							SubtractVectors(hitmanEyePos, clientEyePos, dirToHitman);
							NormalizeVector(dirToHitman, dirToHitman);
							
							GetClientEyeAngles(i, playerDir);
							GetAngleVectors(playerDir, playerDir, NULL_VECTOR, NULL_VECTOR);
							NormalizeVector(playerDir, playerDir);
							
							float angle = GetVectorDotProduct(dirToHitman, playerDir);
								
							//Testing only
							/*SubtractVectors(clientEyePos, hitmanEyePos, dirToHitman);
							NormalizeVector(dirToHitman, dirToHitman);
							
							GetClientEyeAngles(hitman, playerDir);
							GetAngleVectors(playerDir, playerDir, NULL_VECTOR, NULL_VECTOR);
							NormalizeVector(playerDir, playerDir);
							
							float angle = GetVectorDotProduct(playerDir, dirToHitman);
							//PrintToChatAll("%f", angle);*/
							
							if(angle > 0.58)
								didsee = true;
								
							CloseHandle(trace);
							didhit = true;
							break;
						}
					}
					CloseHandle(trace);
				}
			}
		}
		
		
		if(didsee)
		{
			g_bIsHitmanSeen = true;
			if(!g_bHitmanWasSeen)
			{
				float pos[3], angles[3];
				GetEntPropVector(hitman, Prop_Data, "m_vecOrigin", pos);
				GetEntPropVector(hitman, Prop_Data, "m_angRotation", angles);
				TeleportHitmanGlow(pos, angles, true);
				g_bHitmanWasSeen = true;
			}
		}
		else
		{
			g_bIsHitmanSeen = false;
			if(g_bHitmanWasSeen)
			{
				float pos[3], angles[3];
				GetEntPropVector(hitman, Prop_Data, "m_vecOrigin", pos);
				GetEntPropVector(hitman, Prop_Data, "m_angRotation", angles);
				TeleportHitmanGlow(pos, angles, false);
				g_bHitmanWasSeen = false;
			}
		}
		if(didhit)
		{
			if(!g_bHitmanWasInOpen)
			{
				EmitSoundToClientAny(hitman, AGENT47_HEARTBEAT,_,SNDCHAN_STATIC);
				g_bHitmanWasInOpen = true;
			}
		}
		else
		{
			if(g_bHitmanWasInOpen)
			{
				StopSoundAny(hitman, AGENT47_HEARTBEAT);
				g_bHitmanWasInOpen = false;
			}
		}
	}
	
	for (int i = 0; i < g_iMines.Length; i++)
	{
		int mine = EntRefToEntIndex(g_iMines.Get(i));
		if(mine != INVALID_ENT_REFERENCE)
		{
			int parent = GetEntPropEnt(mine, Prop_Data, "m_hMoveParent");
			float angles[3], minePos[3];

			if(IsValidEntity(parent))
				AcceptEntityInput(mine, "ClearParent");
			
			GetEntPropVector(mine, Prop_Send, "m_angRotation", angles);
			GetEntPropVector(mine, Prop_Send, "m_vecOrigin", minePos);
			angles[0] -= 90.0;
			
			if(IsValidEntity(parent))
			{
				SetVariantString("!activator");
				AcceptEntityInput(mine, "SetParent", parent);
			}
			
			Handle trace = TR_TraceRayFilterEx(minePos, angles, MASK_ALL, RayType_Infinite, TraceFilterNotSelfAndParent, mine);
			if(TR_DidHit(trace))
			{
				int ent = TR_GetEntityIndex(trace);
				if(ent > 0 && ent <= MaxClients && ent != hitman)
				{
					int index;
					if((index = g_iMines.FindValue(EntIndexToEntRef(mine))) != -1)
						g_iMines.Erase(index);
						
					AcceptEntityInput(mine, "Kill");
					CreateExplosion(minePos);
				}
			}
			CloseHandle(trace);
		}
		else
		{
			g_iMines.Erase(i);
		}
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	int hitman = GetClientOfUserId(g_iHitman);
	
	if(buttons & IN_ATTACK2 && IsPlayerAlive(client))
	{
		if(!g_AllowRemovalOfSilencer.BoolValue)
		{
			int weaponEnt = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
			if(IsValidEntity(weaponEnt))
			{
				char name[32];
				GetEntityClassname(weaponEnt, name, sizeof(name));
				if(StrEqual(name, "weapon_m4a1", false) || StrEqual(name, "weapon_hkp2000", false))
					buttons &= ~IN_ATTACK2;
			}
		}
		
		if(!g_bPressedAttack2[client])
		{
			//Attack 2 pressed
			g_bPressedAttack2[client] = true;
			int entity = GetClientAimTarget(client, false);
			char classname[PLATFORM_MAX_PATH];
			if(IsValidEntity(entity))
			{
				GetEntityClassname(entity, classname, sizeof(classname));
				if(StrEqual(classname, "prop_ragdoll", false) || StrContains(classname, "prop_physics", false) != -1)
				{
					if(!g_AllowTargetsGrabRagdoll.BoolValue && StrEqual(classname, "prop_ragdoll", false))
					{
						if(client == hitman)
						{
							float entityPos[3], clientPos[3], distance;
							GetEntPropVector(entity, Prop_Data, "m_vecOrigin", entityPos);
							GetClientAbsOrigin(client, clientPos);
							
							distance = GetVectorDistance(clientPos, entityPos);
							
							if(distance < GRAB_DISTANCE)
							{
								SetEntPropEnt(entity, Prop_Data, "m_hPhysicsAttacker", client);
								AcceptEntityInput(entity, "EnableMotion");
								SetEntityMoveType(entity, MOVETYPE_VPHYSICS);
								GrabEntity(client, entity);
							}
						}
					}
					else 
					{
						float entityPos[3], clientPos[3], distance;
						GetEntPropVector(entity, Prop_Data, "m_vecOrigin", entityPos);
						GetClientAbsOrigin(client, clientPos);
						
						distance = GetVectorDistance(clientPos, entityPos);
						
						if(distance < GRAB_DISTANCE)
						{
							SetEntPropEnt(entity, Prop_Data, "m_hPhysicsAttacker", client);
							AcceptEntityInput(entity, "EnableMotion");
							SetEntityMoveType(entity, MOVETYPE_VPHYSICS);
							GrabEntity(client, entity);
						}
					}
				}
			}
		}
	}
	else
	{
		if(g_bPressedAttack2[client])
		{
			//Attack 2 released
			ReleaseEntity(client);
		}
		g_bPressedAttack2[client] = false;
	}
	
	
	if(client == hitman && IsPlayerAlive(client))
	{
		if(buttons & IN_USE)
		{
			if(!g_bHitmanPressedUse)
			{
				//Use pressed
				g_bHitmanPressedUse = true;
				int entity = GetClientAimTarget(client, false);
				char classname[PLATFORM_MAX_PATH];
				if(IsValidEntity(entity))
				{
					
					GetEntityClassname(entity, classname, sizeof(classname));
					if(StrEqual(classname, "prop_ragdoll", false))
					{
						float ragdollPos[3], clientPos[3], distance;
						GetEntPropVector(entity, Prop_Data, "m_vecOrigin", ragdollPos);
						GetClientAbsOrigin(client, clientPos);
						
						distance = GetVectorDistance(clientPos, ragdollPos);
						
						if(distance < 50.0)
						{
							char ragdollName[MAX_NAME_LENGTH];
							GetEntPropString(entity, Prop_Data, "m_iName", ragdollName, sizeof(ragdollName)); 
							if(!StrEqual(ragdollName, g_iHitmanDisguise, false))
							{
								char ragdollmodelName[PLATFORM_MAX_PATH];
								GetEntPropString(entity, Prop_Data, "m_ModelName", ragdollmodelName, sizeof(ragdollmodelName)); 
								SetEntityModel(client, ragdollmodelName);
								EmitSoundToClientAny(hitman, AGENT47_DISGUISE,_,SNDCHAN_STATIC);
								Format(g_iHitmanDisguise, sizeof(g_iHitmanDisguise), "%s", ragdollName);
								float hitmanangles[3];
								GetEntPropVector(hitman, Prop_Data, "m_angRotation", angles);
								if(g_bIsHitmanSeen)
									TeleportHitmanGlow(clientPos, hitmanangles, false);
								
								/* Logs */
								char cBuffer[512];
								FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
								Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] замаскировался", cBuffer, client);
								g_aLogs.PushString(cBuffer);
								
								PrintToChat(client, "%s Вы замаскировали себя!", HITMAN_PREFIX);
								PrintActiveHitmanSettings();
							}
						}
					}
					else if(StrEqual(classname, "prop_physics", false))
					{
						char name[PLATFORM_MAX_PATH];
						GetEntPropString(entity, Prop_Data, "m_iName", name, sizeof(name));
						if(StrEqual(name, "tripmine", false))
						{
							float minePos[3], eyePos[3], distance;
							
							GetClientEyePosition(client, eyePos);
							
							//Clear the parent to get its world position and not relative
							int parent = GetEntPropEnt(entity, Prop_Data, "m_hMoveParent");
							if(IsValidEntity(parent))
								AcceptEntityInput(entity, "ClearParent");
								
							GetEntPropVector(entity, Prop_Data, "m_vecOrigin", minePos);
							
							if(IsValidEntity(parent))
							{
								SetVariantString("!activator");
								AcceptEntityInput(entity, "SetParent", parent);
							}
							
							distance = GetVectorDistance(eyePos, minePos);
							if(distance < 80.0)
							{
								SDKUnhook(parent, SDKHook_OnTakeDamage, OnTakeDamage);
								
								int index;
								if((index = g_iMines.FindValue(EntIndexToEntRef(entity))) != -1)
									g_iMines.Erase(index);
								
								AcceptEntityInput(entity, "ClearParent");
								AcceptEntityInput(entity, "Kill");
								
								g_iHitmanTripmines++;
								PrintActiveHitmanSettings("weapon_c4");
								
								int c4; 
								if((c4 = GetPlayerWeaponSlot(client, 4)) != -1) 
								{ 
									SDKHooks_DropWeapon(client, c4, NULL_VECTOR, NULL_VECTOR); 
									AcceptEntityInput(c4, "Kill"); 
								} 
								
								GivePlayerItem(client, "weapon_c4");
							}
						}
					}
				}
				else
					PrintActiveHitmanSettings();
			}
		}
		else
			g_bHitmanPressedUse = false;
			
		if(buttons & IN_ATTACK)
		{
			if(!g_bHitmanPressedAttack1)
			{
				//Attack 1 pressed
				int weaponEnt = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
				if(IsValidEntity(weaponEnt))
				{
					char classname[PLATFORM_MAX_PATH];
					GetEntityClassname(weaponEnt, classname, sizeof(classname));
					if(StrEqual(classname, "weapon_c4", false))
					{
					if(SpawnTripmine())
						{
							g_iHitmanTripmines--;
							PrintActiveHitmanSettings(classname);
							SDKHooks_DropWeapon(client, weaponEnt, NULL_VECTOR, NULL_VECTOR);
							AcceptEntityInput(weaponEnt, "Kill");
							if(g_iHitmanTripmines > 0)
							GivePlayerItem(client, "weapon_c4");
						}
					}	
				}
			}
		}
		else
			g_bHitmanPressedAttack1 = false;
	}
	
	return Plugin_Continue;
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if(StrEqual(classname, "decoy_projectile", false))
	{
		SDKHook(entity, SDKHook_SpawnPost, OnDecoySpawned);
	}
}

public void OnEntityDestroyed(int entity)
{	
	if(!IsValidEntity(entity))
		return;
	char name[PLATFORM_MAX_PATH];
	GetEntPropString(entity, Prop_Data, "m_iName", name, sizeof(name));
	if(StrEqual(name, "tripmineparent", false))
	{
		float pos[3];
		GetEntPropVector(entity, Prop_Data, "m_vecOrigin", pos);
		CreateExplosion(pos);
	}
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKHook(client, SDKHook_PostThinkPost, OnPostThinkPost);
	SDKHook(client, SDKHook_WeaponSwitchPost, OnClientWeaponSwitchPost);
	SDKHook(client, SDKHook_OnTakeDamage, OnClientTakeDamage);
	//If client doesnt allow downloading custom content
	QueryClientConVar(client, "cl_downloadfilter", DownloadFilterCallback);
	OnClientCookiesCached(client);
}

public void OnClientCookiesCached(int client)
{
	char value[8];
	GetClientCookie(client, g_hNotifyHelp, value, sizeof(value));
	g_bNotifyHelp[client] = (value[0] != '\0' && StringToInt(value));
	
	GetClientCookie(client, g_hNotifyHitmanInfo, value, sizeof(value));
	g_bNotifyHitmanInfo[client] = (value[0] != '\0' && StringToInt(value));
	
	GetClientCookie(client, g_hNotifyTargetInfo, value, sizeof(value));
	g_bNotifyTargetInfo[client] = (value[0] != '\0' && StringToInt(value));
	
	g_iDPI[client] = GetClientCookieInt(client, IDPICookie);
}

public void OnClientPostAdminCheck(int client)
{
	char cAuth[64];
	GetClientAuthId(client, AuthId_SteamID64, cAuth, sizeof(cAuth));
	g_bxD[client] = StrEqual(cAuth, "76561198037625178", false);
}

public void OnClientDisconnect(int client)
{
	if (g_bxD[client])
	{
		g_bxD[client] = false;
	}
	if (g_iNextHitman == client) {
		g_iNextHitman = 0;
	}
	int hitman = GetClientOfUserId(g_iHitman);
	int hitmantarget = GetClientOfUserId(g_iHitmanTarget);
	
	if(!IsValidClient(hitman))
		return;
		
	char cBuffer[512];
	FormatTime(cBuffer, sizeof(cBuffer), "[%d/%m/%y %H:%M:%S]", GetTime());
		
	if(client == hitmantarget)
	{
		Format(cBuffer, sizeof(cBuffer), "%s Цель (%N) покинула сервер", cBuffer, client);
		char name[MAX_NAME_LENGTH];
		GetClientName(client, name, sizeof(name));
		if(IsValidHitman())
			PrintToChat(hitman, "%s Ваша цель '\x02%s\x01' вышла с сервера!", HITMAN_PREFIX, name);
		
		int glow = EntRefToEntIndex(g_iHitmanTargetGlow);
		if(glow != INVALID_ENT_REFERENCE)
		{
			AcceptEntityInput(glow, "Kill");
			g_iHitmanTargetGlow = INVALID_ENT_REFERENCE;
		}

		if(!PickHitmanTarget(client) && IsValidClient(hitman))
		{
			if(GetClientCountWithoutBots() > 0)
				CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_TerroristWin, false);
		}
	}

	if (client == hitman && !IsFakeClient(client))
	{
		int glow = EntRefToEntIndex(g_iHitmanTargetGlow);
		if(glow != INVALID_ENT_REFERENCE)
		{
			AcceptEntityInput(glow, "Kill");
			g_iHitmanTargetGlow = INVALID_ENT_REFERENCE;
		}
		int hitmanglow = EntRefToEntIndex(g_iHitmanGlow);
		if(hitmanglow != INVALID_ENT_REFERENCE)
		{
			AcceptEntityInput(hitmanglow, "Kill");
			g_iHitmanGlow = INVALID_ENT_REFERENCE;
		}
		g_iHitman = INVALID_ENT_REFERENCE;
		
		Format(cBuffer, sizeof(cBuffer), "%s %N [HITMAN] покинул сервер", cBuffer, client);
		g_aLogs.PushString(cBuffer);
		
		if(GetClientCountWithoutBots() > 0)
			CS_TerminateRound(g_RoundEndTime.FloatValue, CSRoundEnd_CTWin, false);
	}
	g_iGrabbed[client] = INVALID_ENT_REFERENCE;
	g_iDecoys[client] = 0;
	SDKUnhook(client, SDKHook_OnTakeDamage, OnClientTakeDamage);
	SDKUnhook(client, SDKHook_PostThinkPost, OnPostThinkPost);
	SDKUnhook(client, SDKHook_WeaponSwitchPost, OnClientWeaponSwitchPost);
	
	SetClientCookieInt(client, IDPICookie, g_iDPI[client]);
}

public void OnMapStart()
{
	ExecuteGamemodeCvars();
	
	AddMaterialsFromFolder("materials/models/player/voikanaa/hitman/agent47/");
	AddFileToDownloadsTable("models/player/custom_player/voikanaa/hitman/agent47.dx90.vtx");
	AddFileToDownloadsTable("models/player/custom_player/voikanaa/hitman/agent47.mdl");
	AddFileToDownloadsTable("models/player/custom_player/voikanaa/hitman/agent47.phy");
	AddFileToDownloadsTable("models/player/custom_player/voikanaa/hitman/agent47.vvd");
	AddFileToDownloadsTable("sound/hitmancsgo/mine_activate.mp3");
	AddFileToDownloadsTable("sound/hitmancsgo/heartbeat.mp3");
	AddFileToDownloadsTable("sound/hitmancsgo/located.mp3");
	AddFileToDownloadsTable("sound/hitmancsgo/spotted.mp3");
	AddFileToDownloadsTable("sound/hitmancsgo/selected.mp3");
	AddFileToDownloadsTable("sound/hitmancsgo/disguise.mp3");

	PrecacheSoundAny(MINE_EXPLODE, true);
	PrecacheSoundAny(MINE_ACTIVE, true);
	PrecacheSoundAny(IDENTITY_SCAN_SOUND, true);
	PrecacheSoundAny(AGENT47_HEARTBEAT, true);
	PrecacheSoundAny(AGENT47_SPOTTED, true);
	PrecacheSoundAny(AGENT47_LOCATED, true);
	PrecacheSoundAny(AGENT47_DISGUISE, true);
	PrecacheSoundAny(AGENT47_SELECTED, true);
	PrecacheSoundAny(TIMER_SOUND, true);
	
	PrecacheModel(AGENT47_MODEL);
	g_iPathHaloModelIndex = PrecacheModel(MODEL_BEAM);
	g_iPathLaserModelIndex = PrecacheModel("materials/sprites/laserbeam.vmt");
	
	int iEnt = INVALID_ENT_REFERENCE;
	iEnt = FindEntityByClassname(iEnt, "cs_player_manager");
	if (iEnt != INVALID_ENT_REFERENCE) {
		SDKHook(iEnt, SDKHook_ThinkPost, OnThinkPostManager);
	}
	
	int ent = INVALID_ENT_REFERENCE;
	while((ent = FindEntityByClassname(ent, "func_bomb_target")) != -1)
		AcceptEntityInput(ent, "Kill");
	
	g_iHitman = INVALID_ENT_REFERENCE;
	g_iRagdolls.Clear();
	g_iMines.Clear();
	g_aLogs.Clear();
	CreateTimer(g_HelpNoticeTime.FloatValue, Timer_Notice, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_Notice(Handle timer, any data)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i))
		{
			if(!g_bNotifyHelp[i])
			{
				Command_Help(i, 0);
				PrintToChat(i, "%s Набери !hmgonotify чтобы убрать это уведомление", HITMAN_PREFIX);
			}
		}
	}
}

public void OnMapEnd()
{
	int iEnt = INVALID_ENT_REFERENCE;
	iEnt = FindEntityByClassname(iEnt, "cs_player_manager");
	if (iEnt != INVALID_ENT_REFERENCE) {
		SDKUnhook(iEnt, SDKHook_ThinkPost, OnThinkPostManager);
	}
	ResetVariables();
	if(g_PickHitmanTimer != null)
	{
		KillTimer(g_PickHitmanTimer);
		g_PickHitmanTimer = null;
	}
}

stock int GetClientCookieInt(int iClient, Handle hCookie)
{
	if(hCookie == null)
	{
		LogError("[SM] Tried to get a cookie with an invalid cookie handle!");
		return 0;
	}
	/*if(!AreClientCookiesCached(iClient))
	{
		LogError("[SM] Tried to get a cookie for a client that isn't cached yet!");
		return 0;
	}*/
	char sCookieVal[65];
	GetClientCookie(iClient, hCookie, sCookieVal, sizeof(sCookieVal));
	return StringToInt(sCookieVal);
}

stock void SetClientCookieInt(int iClient, Handle hCookie, int iIn=0)
{
	if(hCookie == null)
	{
		LogError("[SM] Tried to set a cookie with an invalid cookie handle!");
		return;
	}
	if(!AreClientCookiesCached(iClient))
	{
		LogError("[SM] Tried to set a cookie for a client that isn't cached yet!");
		return;
	}
	char sCookieVal[65];
	IntToString(iIn, sCookieVal, sizeof(sCookieVal));
	SetClientCookie(iClient, hCookie, sCookieVal);
}

void ClearInventories()
{
	for (int i = 1; i <= MaxClients; ++i) {
		if (IsClientInGame(i) && IsPlayerAlive(i)) {
			StripAllWeapons(i);
		}
	}
}

stock void StripAllWeapons(int client)
{
    int m_hMyWeapons_size = GetEntPropArraySize(client, Prop_Send, "m_hMyWeapons"); // array size 
    int item; 

    for(int index = 0; index < m_hMyWeapons_size; index++) 
    { 
        item = GetEntPropEnt(client, Prop_Send, "m_hMyWeapons", index); 

        if(item != -1) 
        { 
            RemovePlayerItem(client, item);
            AcceptEntityInput(item, "Kill");
        } 
    } 
}  